export interface Project {
  id: number;
  name: string;
  company: string;
  image: string
}

export interface CreateProjectDto {
  name: string;
  company: string;
  image: string
}

export interface UpdateProjectDto {
  name?: string;
  company?: string;
  image?: string
}
