export interface User {
  id: string;
  firstname: string;
  lastname:  string;
  email: string;
  status:  string;
  profile: string;
  lastLogin:  string;
  company: string;
}

export interface CreateUserDto {
  firstname: string;
  lastname: string;
  email: string;
  profile: number;
  company: number;
}

export interface UpdateUserDto {
  firstname?: string;
  lastname?: string;
  email?: string;
  profile?: number;
  company?: number;
}