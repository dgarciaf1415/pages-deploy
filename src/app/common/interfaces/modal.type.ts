export interface ModalOpen {
  type?: string;
  display: boolean;
}

export interface ModalClose {
  close: boolean;
  reason: string;
  type: string;
  form: string;
}

export interface ModalOptions {
  title?: string;
  buttonText?: string;
  enableButton?: boolean;
  itemType?: string;
  itemName?: string;
  description?: string;
}