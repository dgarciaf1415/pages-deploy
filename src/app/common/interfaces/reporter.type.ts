export interface Reporter {
  id: number;
  name: string;
  email: string;
  phone: string;
  organization: string;
  status: string;
}

export interface CreateReporterDto {
  name: string;
  email: string;
  phone: string;
  organization: string;
  status: string;
}

export interface UpdateReporterDto {
  name?: string;
  email?: string;
  phone?: string;
  organization?: string;
  status?: string;
}
