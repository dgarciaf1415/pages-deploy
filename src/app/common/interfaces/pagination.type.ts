export interface PaginationOptions {
  limit: number;
  offset: number;
  total: number;
}
