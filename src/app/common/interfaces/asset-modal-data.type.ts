export interface AssetModalData {
  asset: string;
  assetType: string;
  assetDescription: string;
  assetSamples: number;
  assetSamplesAverage: number;
  commentsQuantity: number;
  display: boolean;
  reporters: AssetModalReportersData[];
  wasView: boolean;
}

export interface AssetModalReportersData {
  name: string;
  date: Date;
  comment: string;
}

