export interface Company {
  id: number;
  name: string;
  status: string;
  type: string;
  companies: Company[];
  hasCompanies?: Company[];
}

export interface CreateCompanyDto {
  name: string;
  status: string;
  type: string;
  companies: Company[];
  hasCompanies?: Company[];
}

export interface UpdateCompanyDto {
  name?: string;
  status?: string;
  type?: string;
  companies?: Company[];
  hasCompanies?: Company[];
}

export enum CompanyType {
  ADMIN = 'ADMIN',
  AGENCY = 'AGENCY',
  CLIENT = 'BRAND',
}
