export interface CommentModalData {
  reporterName: string;
  date: Date;
  display: boolean;
  images: ImageAndComment[];
}

export interface ImageAndComment {
  id: string;
  comment: string;
  image: string;
}

export interface ImageData {
  id: string;
  image: string;
}

export interface CommentData {
  id: string;
  comment: string;
}

