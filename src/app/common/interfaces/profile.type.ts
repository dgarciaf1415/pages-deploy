export interface Profile {
  id: number;
  name: string;
  status: string;
}

export interface CreateProfileDto {}

export interface UpdateProfileDto {}

export enum ProfileType {
  SUPERADMIN = 'SUPERADMIN',
  ADMIN = 'ONXITE MASTER ADMIN',
  MANAGER = 'ONXITE ADMIN',
  CLIENT = 'ADMIN',
}
