export interface Survey {
  id: number;
  name: string;
  status: string;
}

export interface CreateSurveyDto {
  name: string;
  status: string;
}

export interface UpdateSurveyDto {
  name?: string;
  status?: string;
}

export interface Questions {
  id: number;
  name: string;
  allowAttachments: boolean;
  allowComments: boolean;
}