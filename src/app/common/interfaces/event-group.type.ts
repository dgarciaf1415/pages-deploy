export interface EventGroup {
  id: number;
  name: string;
  startDate: string;
  endDate: string;
  status: string;
  platformUrl: string;
  project: number;
  eventType: number;
  eventVenue: number;
  eventVenuePlatform: number;
  location: string;
}

export interface CreateEventGroupDto {
  name: string;
  startDate: string;
  endDate: string;
  status: string;
  platformUrl: string;
  project: number;
  eventType: number;
  eventVenue: number;
  eventVenuePlatform: number;
  location: string;
}

export interface UpdateEventGroupDto {
  name?: string;
  startDate?: string;
  endDate?: string;
  status?: string;
  platformUrl?: string;
  project?: number;
  eventType?: number;
  eventVenue?: number;
  eventVenuePlatform?: number;
  location?: string;
}
