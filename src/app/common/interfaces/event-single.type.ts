export interface Event {
  name: string;
  startDate: string;
  endDate: string;
  startTime?: string;
  endTime?: string;
  budget: number;
  attendance: number;
  status: string;
  eventGroup: number;
  eventObjetive: number;
  eventSecondaryObjetive: number;
  targets: Target[];
  assets: Asset[];
}

export interface Target {
  expectedAttendance: number;
  eventAgeMin: number;
  eventAgeMax: number;
  eventGender: number;
  eventEthnicity: number;
}

export interface Asset {
  name: string;
  type: number;
}

export interface CreateEventDto extends Event {}

export interface UpdateEventDto {
  name?: string;
  startDate?: string;
  endDate?: string;
  budget?: number;
  attendance?: number;
  status?: string;
  eventGroup?: number;
  eventObjetive?: number;
  eventSecondaryObjetive?: number;
  targets?: Target[];
  assets?: Asset[];
}
