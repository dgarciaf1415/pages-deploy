export interface Menu {
  id: number;
  name: string;
  url: string;
  description: string;
  icon: string;
  position: number;
  slug: string;
  i18n: string;
  type: string;
  status: string;
  menu: number;
}

export interface CreateMenuDto {
  name: string;
  url: string;
  description: string;
  icon: string;
  position: number;
  slug: string;
  i18n: string;
  type: string;
  status: string;
  admMenu: number;
}

export interface UpdateMenuDto {
  name?: string;
  url?: string;
  description?: string;
  icon?: string;
  position?: number;
  slug?: string;
  i18n?: string;
  type?: string;
  status?: string;
  admMenu?: number;
}
