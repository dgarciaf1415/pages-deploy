export interface EventReport {
	objetive: String;
	eventType: String;
	location: String;
	dateStart: String;
	dateFinish: String;
	attendance: Number;
	attendanceExpectedPerTarget: AttendanceExpectedPerTarget[];
	totalReporters: Number;
	reportersAttended: Number;
	brandAssets: Asset[];
	reporterComments: Comment[];
	reporterAttachments: Attachment[];
}

export interface AttendanceExpectedPerTarget {
	attendance: Number;
	ageMin: Number;
	ageMax: Number;
	gender: String;
	demographic: String;
}
export interface Asset {
	type: String;
	url: String;
}
export interface Comment {}
export interface Attachment {}
