export interface Activity {
  controller: string;
  method: string;
  detail: string;
  ip: string;
  user: string;
  createdAt: string;
}
