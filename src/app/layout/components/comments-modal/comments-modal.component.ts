import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { NgbCarousel, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { CommentsModalService } from "./comments-modal.service";
import { CommentData, CommentModalData, ImageData } from "app/common/interfaces/comment-modal-data.type";

@Component({
  selector: "app-comments-modal",
  templateUrl: "./comments-modal.component.html",
  styleUrls: ["./comments-modal.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CommentsModalComponent implements OnInit {
  @ViewChild("commentModal") commentModal: ElementRef;
  @ViewChild('imagesCarousel') imagesCarousel: NgbCarousel;

  public carouselImages: ImageData[];
  public comments: CommentData[];
  public imageSelected: string;
  public reporterName: string;
  public publicationDate: string | Date;

  // Private
  private _unsubscribeAll: Subject<any>

  constructor(
    private modal: NgbModal,
    private commentModalService: CommentsModalService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.commentModalService.modalStatus
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: CommentModalData) => {
        if (data.display) {
          this.reporterName = data.reporterName;
          this.publicationDate = data.date;
          this.comments = data.images.map((item) => {
            return {
              id: item.id,
              comment: item.comment
            };
          });
          this.carouselImages = data.images.map((item) => {
            return {
              id: item.id,
              image: item.image,
            };
          })
          this.imageSelected = this.carouselImages[0].id;

          this.modal.open(this.commentModal, {
            centered: true,
            windowClass: 'modal modal-primary ox--comment-modal',
            size: 'xl'
          });
        } else {
          this.modal.dismissAll();
        }
      });
  }

  selectImage(imageId: string) {
    this.imageSelected = imageId;
    this.imagesCarousel.select(this.imageSelected);
    this.imagesCarousel.activeId = this.imageSelected;
  }

  loadCarousel(carousel: NgbCarousel) {
    this.imagesCarousel = carousel;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
