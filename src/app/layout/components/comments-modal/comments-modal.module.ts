import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommentsModalComponent } from './comments-modal.component';
import { CommentsModalService } from './comments-modal.service';
import { UtilsModule } from 'app/utils/utils.module';

@NgModule({
  declarations: [
    CommentsModalComponent
  ],
  exports: [ CommentsModalComponent ],
  imports: [
    CommonModule,
    NgbModule,
    UtilsModule,
  ],
  providers: [ CommentsModalService ],
})
export class CommentsModalModule { }
