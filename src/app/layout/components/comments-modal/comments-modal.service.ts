import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ModalOpen } from 'app/common/interfaces/modal.type';
import { CommentModalData } from 'app/common/interfaces/comment-modal-data.type';

@Injectable({
  providedIn: 'root'
})
export class CommentsModalService {
  private display = new BehaviorSubject<ModalOpen>({ display: false });
  public modalStatus = this.display.asObservable();

  open(data: CommentModalData) {
    this.display.next(data);
  }

  close() {
    this.display.next({ display: false });
  }
}
