import { TestBed } from '@angular/core/testing';

import { CommentsModalService } from './comments-modal.service';

describe('CommentsModalService', () => {
  let service: CommentsModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommentsModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
