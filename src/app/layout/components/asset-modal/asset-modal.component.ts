import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { AssetModalService } from "./asset-modal.service";
import { AssetModalData } from "app/common/interfaces/asset-modal-data.type";

@Component({
  selector: "app-asset-modal",
  templateUrl: "./asset-modal.component.html",
  styleUrls: ["./asset-modal.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AssetModalComponent implements OnInit {
  @ViewChild("assetModal") assetModal: ElementRef;

  public assetData: any;
  public commentsQuantity: number;
  public samplesAverage: number;
  public samplesPercentage: number;
  public samplesTotal: number;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private modal: NgbModal,
    private assetModalService: AssetModalService
  ) {
    this._unsubscribeAll = new Subject();
    this.samplesTotal = 0;
  }

  ngOnInit(): void {
    this.assetModalService.modalStatus
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: AssetModalData) => {
        this.assetData = data;

        if (data.display) {
          this.commentsQuantity = data.commentsQuantity;
          this.samplesAverage = Number(data.assetSamplesAverage);
          this.samplesPercentage =
            (this.samplesAverage * 100) / this.assetData.assetSamples;

          this.modal.open(this.assetModal, {
            centered: true,
            windowClass: "modal modal-primary ox--asset-modal",
          });
        } else {
          this.modal.dismissAll();
        }
      });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
