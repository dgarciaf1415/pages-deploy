import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AssetModalComponent } from './asset-modal.component';
import { UtilsModule } from 'app/utils/utils.module';
import { AssetModalService } from './asset-modal.service';

@NgModule({
  declarations: [
    AssetModalComponent
  ],
  exports: [AssetModalComponent],
  imports: [
    CommonModule,
    NgbModule,
    UtilsModule,
  ],
  providers: [ AssetModalService ],
})
export class AssetModalModule { }
