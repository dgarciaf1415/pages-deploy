import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { AssetModalData } from 'app/common/interfaces/asset-modal-data.type';
import { ModalOpen } from 'app/common/interfaces/modal.type';

@Injectable({
  providedIn: 'root'
})
export class AssetModalService {
  private display = new BehaviorSubject<ModalOpen>({ display: false });
  public modalStatus = this.display.asObservable();

  open(data: AssetModalData) {
    this.display.next(data);
  }

  close() {
    this.display.next({ display: false });
  }
}
