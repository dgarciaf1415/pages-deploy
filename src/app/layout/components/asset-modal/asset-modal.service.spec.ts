import { TestBed } from '@angular/core/testing';

import { AssetModalService } from './asset-modal.service';

describe('AssetModalService', () => {
  let service: AssetModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssetModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
