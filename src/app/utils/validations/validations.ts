import { FormControl, FormGroup, ValidatorFn } from '@angular/forms';

export class Validations {
  constructor() {}

  /**
   * Validates if two fields value are same
   * @param controlName field with original value
   * @param matchingControlName field with confirm value
   */
  static compareFields(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  /**
   * Validates at least one of checkbox group is checked
   * @param minRequired checkbox quantity for validate
   */
  static requireCheckboxesToBeCheckedValidator(minRequired: number = 1): ValidatorFn {
    return (formGroup: FormGroup) => {
      let checked = 0;

      Object.keys(formGroup.controls).forEach((key) => {
        const control = formGroup.controls[key];

        if (control.value === true) {
          checked++;
        }
      });

      if (checked < minRequired) {
        return {
          requireCheckboxesToBeChecked: true,
        };
      }

      return null;
    };
  }

  /**
   * Validates if the end time value is less than start time on the same date
   * @param startTimeField field with start time value
   * @param endTimeField field with end time value
   */
  static compareTimes(startTimeField: string, endTimeField: string) {
    return (formGroup: FormGroup) => {
      const startTime = formGroup.controls[startTimeField];
      const endTime = formGroup.controls[endTimeField];
      const startDate = startTime.parent?.controls['startDate'];
      const endDate = startTime.parent?.controls['endDate'];

      if (startDate && endDate) {
        const startDateValue = startDate.value;
        const endDateValue = endDate.value;

        if ((startDateValue && endDateValue) && (startDateValue.year === endDateValue.year) && (startDateValue.month === endDateValue.month) && (startDateValue.day === endDateValue.day)) {
          if (endTime.errors && !endTime.errors.wrongDateSelection) {
            return;
          }
          
          if ((endTime.value?.hour <= startTime.value?.hour) && (endTime.value?.minute <= startTime.value?.minute)) {
            return endTime.setErrors({ wrongDateSelection: true });
          } else {
            return endTime.setErrors(null);
          }
        }
      }

      return endTime.setErrors(null);
    };
  };
}
