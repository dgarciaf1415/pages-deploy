import { CoreMenu } from "@core/types";

export const menu: CoreMenu[] =
  !!localStorage.getItem("currentUser") &&
  !!JSON.parse(localStorage.getItem("currentUser")).menus
    ? [JSON.parse(localStorage.getItem("currentUser")).menus]
    : [];

// export const menu: CoreMenu[] = []
