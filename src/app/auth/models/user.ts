﻿export class User {
  access_token: string;
  user: UserInfo;
  menus: Menu;
}

export class UserInfo {
  id: string;
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
  avatar: string;
  activationHash: string;
  expirationHash: string;
  status: string;
  lastLogin: Date;
  createdAt: Date;
  updatedAt: Date;
  profile: Profile;
  company: Company;
}

export class Profile {
  id: number;
  name: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

export class Company {
  id: number;
  name: string;
  status: string;
  type: string;
  createdAt: Date;
  updatedAt: Date;
}

export class Menu {
  id: string;
  title: string;
  translate: string;
  type: string;
  icon: string;
  children: Item[];
}

export class Item {
  id: string;
  title: string;
  translate: string;
  type: string;
  icon: string;
}