import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { catchError, takeUntil } from "rxjs/operators";
import { of, Subject } from "rxjs";

import { CoreConfigService } from "@core/services/config.service";
import { UsersService } from "app/main/admin/users/users.service";
import { ActivatedRoute } from "@angular/router";
import { Validations } from "app/utils/validations/validations";

@Component({
  selector: "app-auth-reset-password-v1",
  templateUrl: "./auth-reset-password-v1.component.html",
  styleUrls: ["./auth-reset-password-v1.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AuthResetPasswordV1Component implements OnInit {
  // Public
  public activationHash: string;
  public coreConfig: any;
  public currentUser: string;
  public errorHash: object;
  public passwordTextType: boolean;
  public confPasswordTextType: boolean;
  public isLoading: boolean;
  public resetPasswordForm: FormGroup;
  public submitted = false;
  public submitClicked: boolean;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _coreConfigService: CoreConfigService,
    private _formBuilder: FormBuilder,
    private _usersService: UsersService,
    private route: ActivatedRoute
  ) {
    this._unsubscribeAll = new Subject();

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true,
        },
        menu: {
          hidden: true,
        },
        footer: {
          hidden: true,
        },
        customizer: false,
        enableLocalStorage: false,
      },
    };

    if (this.route.snapshot.paramMap.get("hash")) {
      this.activationHash = this.route.snapshot.paramMap.get("hash");
      this._usersService
        .findByActivationHash(this.activationHash)
        .pipe(
          takeUntil(this._unsubscribeAll),
          catchError((err) => {
            return of(err);
          })
        )
        .subscribe((res: any) => {
          if (res.error) {
            this.errorHash = {
              message: res.error?.message,
              status: res.status,
            };
          } else {
            this.currentUser = res.id;
          }
        });
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetPasswordForm.controls;
  }

  /**
   * Toggle password
   */
  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  /**
   * Toggle confirm password
   */
  toggleConfPasswordTextType() {
    this.confPasswordTextType = !this.confPasswordTextType;
  }

  /**
   * On Submit
   */
  onSubmit() {
    this.submitClicked = true;

    // stop here if form is invalid
    if (this.resetPasswordForm.valid) {
      this.isLoading = true;
      const data = {
        user: this.currentUser,
        password: this.resetPasswordForm.value.newPassword,
        hash: this.activationHash,
      };

      this._usersService
        .resetPassword(data)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
          this.isLoading = false;
          this.submitted = true;

          return this.submitted;
        });
    }
    return;
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.resetPasswordForm = this._formBuilder.group({
      newPassword: ["", [Validators.required]],
      confirmPassword: ["", [Validators.required]],
    }, {
      validator: Validations.compareFields('newPassword', 'confirmPassword')
    });

    // Subscribe to config changes
    this._coreConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.coreConfig = config;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
