import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ModalOptions, ModalClose } from "app/common/interfaces/modal.type";
import { Menu } from "app/common/interfaces/menu.type";
import { MenusService } from "./menus.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Component({
  selector: "app-menus",
  templateUrl: "./menus.component.html",
  styleUrls: ["./menus.component.scss"],
})
export class MenusComponent implements OnInit {
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formMenu: FormGroup;
  public menus: Menu[];
  public menuStatus: { value: string }[];
  public pagination: PaginationOptions;
  public searchItem: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _menusService: MenusService,
    private _modalService: ModalService
  ) {
    this._unsubscribeAll = new Subject();

    this.formMenu = this.fb.group({
      id: [],
      name: ["", [Validators.required]],
      url: [""],
      description: [],
      icon: ["", [Validators.required]],
      position: ["", [Validators.required]],
      slug: ["", [Validators.required]],
      i18n: ["", [Validators.required]],
      type: ["", [Validators.required]],
      status: ["ACTIVE"],
      menu: [""],
    });
    this.menuStatus = [{ value: "ACTIVE" }, { value: "INACTIVE" }];
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Menus",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formMenu.valueChanges.subscribe(() => {
      if (this.formMenu.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadMenus(this.pagination);
  }

  loadMenus(pagination: PaginationOptions) {
    this._menusService.getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.menus = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  modalOpen(type: string, menuId: number) {
    this._modalService.open(type);
    if (menuId) {
      if (type === "edit") {
        this._menusService
          .getById(menuId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((menu) => {
            this.formMenu.get("id").setValue(menu.id);
            this.formMenu.get("name").setValue(menu.name);
            this.formMenu.get("url").setValue(menu.url);
            this.formMenu.get("description").setValue(menu.description);
            this.formMenu.get("icon").setValue(menu.icon);
            this.formMenu.get("position").setValue(menu.position);
            this.formMenu.get("slug").setValue(menu.slug);
            this.formMenu.get("i18n").setValue(menu.i18n);
            this.formMenu.get("type").setValue(menu.type);
            this.formMenu.get("status").setValue(menu.status);
            this.formMenu.get("menu").setValue(menu.menu || "");
          });
        this._modalService.options(
          (this.displayModal = {
            title: "Edit menu",
            buttonText: "Update",
            enableButton: false,
          })
        );
      } else {
        this.formMenu.get("id").setValue(menuId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete menu",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "menu",
            itemName: this.menus.find((menu) => {
              return menu.id === menuId;
            }).name,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create menu",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        delete this.formMenu.value.id;
        this.formMenu.value.menu = this.formMenu.value.menu || null;
        this._menusService
          .create(this.formMenu.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadMenus(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        this._menusService
          .update(
            this.formMenu.get("id").value,
            this.getDirtyValues(this.formMenu)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadMenus(this.pagination);
          });
      } else if (closeValues.type === "delete") {
        this._menusService
          .delete(this.formMenu.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadMenus(this.pagination);
          });
      }
    }

    this.formMenu.reset();
    this.formMenu.get("menu").setValue("");
    this.formMenu.get("status").setValue("ACTIVE");

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  paginate(pagination: PaginationOptions) {
    this.loadMenus(pagination);
  }

  search() {
    this._menusService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.menus = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
