import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "environments/environment";
import {
  Menu,
  CreateMenuDto,
  UpdateMenuDto,
} from "app/common/interfaces/menu.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Injectable({
  providedIn: "root",
})
export class MenusService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/menus`;
  }

  getAll(pagination: PaginationOptions) {
    const url = `${this.apiUrl}?limit=${pagination.limit}&offset=${pagination.offset}`;
    return this.http.get<Menu[]>(url);
  }

  getById(menuId: number) {
    const url = `${this.apiUrl}/${menuId}`;
    return this.http.get<Menu>(url);
  }

  search(pagination: PaginationOptions, search: string) {
    const url = `${this.apiUrl}/search?limit=${pagination.limit}&offset=${pagination.offset}&search=${search}`;
    return this.http.get<Menu[]>(url);
  }

  create(body: CreateMenuDto) {
    const url = this.apiUrl;
    return this.http.post(url, body);
  }

  update(menuId: number, body: UpdateMenuDto) {
    const url = `${this.apiUrl}/${menuId}`;
    return this.http.put(url, body);
  }

  delete(menuId: number) {
    const url = `${this.apiUrl}/${menuId}`;
    return this.http.put(url, { status: "DELETED" });
  }
}
