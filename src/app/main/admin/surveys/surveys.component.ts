import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ModalOptions, ModalClose } from "app/common/interfaces/modal.type";
import { Survey } from "app/common/interfaces/survey.type";
import { SurveysService } from "./surveys.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Component({
  selector: "app-surveys",
  templateUrl: "./surveys.component.html",
  styleUrls: ["./surveys.component.scss"],
})
export class SurveysComponent implements OnInit {
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formSurvey: FormGroup;
  public pagination: PaginationOptions;
  public surveys: Survey[];
  public surveyStatus: { value: string }[];
  public searchItem: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _surveysService: SurveysService,
    private _modalService: ModalService
  ) {
    this._unsubscribeAll = new Subject();

    this.formSurvey = this.fb.group({
      id: [],
      name: ["", [Validators.required]],
      status: ["ACTIVE"],
    });
    this.surveyStatus = [{ value: "ACTIVE" }, { value: "INACTIVE" }];
    
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Surveys",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formSurvey.valueChanges.subscribe(() => {
      if (this.formSurvey.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadSurveys(this.pagination);
  }

  loadSurveys(pagination: PaginationOptions) {
    this._surveysService.getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.surveys = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  modalOpen(type: string, surveyId: number) {
    this._modalService.open(type);
    if (surveyId) {
      if (type === "edit") {
        this._surveysService
          .getById(surveyId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((survey) => {
            this.formSurvey.get("id").setValue(survey.id);
            this.formSurvey.get("name").setValue(survey.name);
            this.formSurvey.get("status").setValue(survey.status);
          });
        this._modalService.options(
          (this.displayModal = {
            title: "Edit survey",
            buttonText: "Update",
            enableButton: false,
          })
        );
      } else {
        this.formSurvey.get("id").setValue(surveyId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete survey",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "survey",
            itemName: this.surveys.find((survey) => {
              return survey.id === surveyId;
            }).name,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create survey",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        delete this.formSurvey.value.id;
        this._surveysService
          .create(this.formSurvey.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadSurveys(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        this._surveysService
          .update(
            this.formSurvey.get("id").value,
            this.getDirtyValues(this.formSurvey)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadSurveys(this.pagination);
          });
      } else if (closeValues.type === "delete") {
        this._surveysService
          .delete(this.formSurvey.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadSurveys(this.pagination);
          });
      }
    }

    this.formSurvey.reset();
    this.formSurvey.get("status").setValue("ACTIVE");

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  paginate(pagination: PaginationOptions) {
    this.loadSurveys(pagination);
  }

  search() {
    this._surveysService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.surveys = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
