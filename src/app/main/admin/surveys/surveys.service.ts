import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "environments/environment";
import {
  Survey,
  CreateSurveyDto,
  UpdateSurveyDto,
} from "app/common/interfaces/survey.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Injectable({
  providedIn: "root",
})
export class SurveysService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/surveys`;
  }

  getAll(pagination: PaginationOptions) {
    const url = `${this.apiUrl}?limit=${pagination.limit}&offset=${pagination.offset}`;
    return this.http.get<Survey[]>(url);
  }

  getById(surveyId: number) {
    const url = `${this.apiUrl}/${surveyId}`;
    return this.http.get<Survey>(url);
  }

  search(pagination: PaginationOptions, search: string) {
    const url = `${this.apiUrl}/search?limit=${pagination.limit}&offset=${pagination.offset}&search=${search}`;
    return this.http.get<Survey[]>(url);
  }

  create(body: CreateSurveyDto) {
    const url = this.apiUrl;
    return this.http.post(url, body);
  }

  update(surveyId: number, body: UpdateSurveyDto) {
    const url = `${this.apiUrl}/${surveyId}`;
    return this.http.put(url, body);
  }

  delete(surveyId: number) {
    const url = `${this.apiUrl}/${surveyId}`;
    return this.http.put(url, { status: "DELETED" });
  }
}
