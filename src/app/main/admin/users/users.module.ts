import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { ModalModule } from "app/layout/components/modal/modal.module";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { CompaniesModule } from "../companies/companies.module";
import { ProfilesModule } from "../profiles/profiles.module";
import { UsersComponent } from "./users.component";
import { UsersService } from "./users.service";

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CoreCommonModule,
    ContentHeaderModule,
    NgbModule,
    ModalModule,
    CompaniesModule,
    ProfilesModule,
    PaginationModule,
  ],
  providers: [UsersService],
})
export class UsersModule {}
