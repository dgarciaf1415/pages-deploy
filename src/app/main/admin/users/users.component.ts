import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";

import { ModalClose, ModalOptions } from "app/common/interfaces/modal.type";
import { CompaniesService } from "../companies/companies.service";
import { Company } from "app/common/interfaces/company.type";
import { Profile, ProfileType } from "app/common/interfaces/profile.type";
import { ProfilesService } from "../profiles/profiles.service";
import { UsersService } from "./users.service";
import { User } from "app/common/interfaces/user.type";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";
import { AuthenticationService } from "app/auth/service";
import { Validations } from "app/utils/validations/validations";
import { Password } from "app/utils/functionalities/password";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent implements OnInit {
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formUser: FormGroup;
  public formUserPassword: FormGroup;
  public pagination: PaginationOptions;
  public passwordForm: boolean;
  public passwordTextType: boolean;
  public confPasswordTextType: boolean;
  public searchItem: string;
  public userLogged: string;
  public users: User[];
  public userStatus: { value: string }[];
  public profileTypes: any;

  public $companies: Observable<Company[]>;
  public $profiles: Observable<Profile[]>;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _companiesService: CompaniesService,
    private _profilesService: ProfilesService,
    private _usersService: UsersService,
    private _modalService: ModalService,
    private _authenticationService: AuthenticationService,
    private passwords: Password,
  ) {
    this._unsubscribeAll = new Subject();
    this.userLogged = (this._authenticationService.currentUserValue as any).user.id;
    this.userStatus = [
      { value: "ACTIVE" },
      { value: "INACTIVE" },
      { value: "BLOCKED" },
    ];

    this.formUser = this.fb.group({
      id: [""],
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      profile: ["", Validators.required],
      company: ["", Validators.required],
      status: ["INACTIVE"],
    });

    this.formUserPassword = this.fb.group({
      user: [],
      password: ["", Validators.required],
      confirmPassword: ["", Validators.required],
    }, {
      validator: Validations.compareFields('password', 'confirmPassword')
    });

    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };

    this.profileTypes = ProfileType;
  }

  get f() {
    return this.formUserPassword.controls;
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Users",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formUser.valueChanges.subscribe(() => {
      if (this.formUser.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.formUserPassword.valueChanges.subscribe(() => {
      if (this.formUserPassword.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadUsers(this.pagination);
  }

  loadUsers(pagination: PaginationOptions) {
    this._usersService
      .getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.users = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  modalOpen(type: string, userId: string, isPassword?: boolean) {
    this._modalService.open(type);
    if (userId) {
      if (type === "edit") {
        if (isPassword) {
          this.passwordForm = true;
          this.formUserPassword.get("user").setValue(userId);
          this._modalService.options(
            (this.displayModal = {
              title: "Change password",
              buttonText: "Update",
              enableButton: false,
              itemType: "user",
              itemName: "",
            })
          );
        } else {
          this._usersService
          .getById(userId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((user) => {
            this.formUser.get("id").setValue(user.id);
            this.formUser.get("firstname").setValue(user.firstname);
            this.formUser.get("lastname").setValue(user.lastname);
            this.formUser.get("email").setValue(user.email);
            this.formUser.get("email").disable();
            this.formUser.get("profile").setValue((user.profile as any).id);
            this.formUser.get("company").setValue((user.company as any).id);
            this.formUser.get("status").setValue(user.status);
          });
          this._modalService.options(
            (this.displayModal = {
              title: "Edit user",
              buttonText: "Update",
              enableButton: false,
            })
          );
        }
      } else {
        this.formUser.get("id").setValue(userId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete user",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "user",
            itemName: this.users.find((user) => {
              return user.id === userId;
            }).email,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create user",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }

    this.$profiles = this._profilesService
      .getAll()
      .pipe(map((res: any) => res.rows));
    this.$companies = this._companiesService
      .getAll()
      .pipe(map((res: any) => res.rows));
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        this._usersService
          .create(this.formUser.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadUsers(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        if (this.passwordForm) {
          delete this.formUserPassword.value.confirmPassword
          this._usersService
            .changePassword(
              this.formUserPassword.value,
            )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.loadUsers(this.pagination);
            });
        } else {
          this._usersService
            .update(
              this.formUser.get("id").value,
              this.getDirtyValues(this.formUser)
            )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.loadUsers(this.pagination);
            });
        }
      } else if (closeValues.type === "delete") {
        this._usersService
          .delete(this.formUser.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadUsers(this.pagination);
          });
      }
    }

    this.formUser.reset();
    this.formUser.get("profile").setValue("");
    this.formUser.get("company").setValue("");
    this.formUser.get("email").enable();
    this.formUser.get("status").setValue("INACTIVE");

    this.formUserPassword.reset();
    this.passwordForm = false;

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  toggleConfPasswordTextType() {
    this.confPasswordTextType = !this.confPasswordTextType;
  }

  generatePassword() {
    const password = this.passwords.generate(16);

    this.formUserPassword.get('password').setValue(password);
    this.formUserPassword.get('confirmPassword').setValue(password);
  }

  paginate(pagination: PaginationOptions) {
    this.loadUsers(pagination);
  }

  search() {
    this._usersService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.users = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
