import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ActivitiesModule } from "./activities/activities.module";
import { CompaniesModule } from "./companies/companies.module";
import { MenusModule } from "./menus/menus.module";
import { ProfilesModule } from "./profiles/profiles.module";
import { ReportersModule } from "./reporters/reporters.module";
import { SurveysModule } from "./surveys/surveys.module";
import { UsersModule } from "./users/users.module";
import { ActivitiesComponent } from "./activities/activities.component";
import { UsersComponent } from "./users/users.component";
import { CompaniesComponent } from "./companies/companies.component";
import { MenusComponent } from "./menus/menus.component";
import { ProfilesComponent } from "./profiles/profiles.component";
import { ReportersComponent } from "./reporters/reporters.component";
import { SurveysComponent } from "./surveys/surveys.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "users",
    pathMatch: "full",
  },
  {
    path: "activities",
    component: ActivitiesComponent,
    data: { animation: "misc" },
  },
  {
    path: "accounts",
    component: CompaniesComponent,
    data: { animation: "misc" },
  },
  {
    path: "menus",
    component: MenusComponent,
    data: { animation: "misc" },
  },
  {
    path: "profiles",
    component: ProfilesComponent,
    data: { animation: "misc" },
  },
  {
    path: "reporters",
    component: ReportersComponent,
    data: { animation: "misc" },
  },
  {
    path: "surveys",
    component: SurveysComponent,
    data: { animation: "misc" },
  },
  {
    path: "users",
    component: UsersComponent,
    data: { animation: "misc" },
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    ActivitiesModule,
    CompaniesModule,
    MenusModule,
    ProfilesModule,
    ReportersModule,
    SurveysModule,
    UsersModule,
  ],
})
export class AdminModule {}
