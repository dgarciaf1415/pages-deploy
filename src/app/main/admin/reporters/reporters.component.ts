import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ModalOptions, ModalClose } from "app/common/interfaces/modal.type";
import { Reporter } from "app/common/interfaces/reporter.type";
import { ReportersService } from "./reporters.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Component({
  selector: "app-reporters",
  templateUrl: "./reporters.component.html",
  styleUrls: ["./reporters.component.scss"],
})
export class ReportersComponent implements OnInit {
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formReporter: FormGroup;
  public pagination: PaginationOptions;
  public reporters: Reporter[];
  public reporterStatus: { value: string }[];
  public searchItem: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _reportersService: ReportersService,
    private _modalService: ModalService
  ) {
    this._unsubscribeAll = new Subject();

    this.formReporter = this.fb.group({
      id: [],
      name: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      phone: [""],
      organization: ["", [Validators.required]],
      status: ["ACTIVE"],
    });
    this.reporterStatus = [{ value: "ACTIVE" }, { value: "INACTIVE" }];
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Reporters",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formReporter.valueChanges.subscribe(() => {
      if (this.formReporter.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadReporters(this.pagination);
  }

  loadReporters(pagination: PaginationOptions) {
    this._reportersService.getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.reporters = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  modalOpen(type: string, reporterId: number) {
    this._modalService.open(type);
    if (reporterId) {
      if (type === "edit") {
        this._reportersService
          .getById(reporterId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((reporter) => {
            this.formReporter.get("id").setValue(reporter.id);
            this.formReporter.get("name").setValue(reporter.name);
            this.formReporter.get("email").setValue(reporter.email);
            this.formReporter.get("email").disable();
            this.formReporter.get("phone").setValue(reporter.phone);
            this.formReporter
              .get("organization")
              .setValue(reporter.organization);
            this.formReporter.get("status").setValue(reporter.status);
          });
        this._modalService.options(
          (this.displayModal = {
            title: "Edit reporter",
            buttonText: "Update",
            enableButton: false,
          })
        );
      } else {
        this.formReporter.get("id").setValue(reporterId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete reporter",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "reporter",
            itemName: this.reporters.find((reporter) => {
              return reporter.id === reporterId;
            }).email,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create reporter",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        delete this.formReporter.value.id;
        this._reportersService
          .create(this.formReporter.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadReporters(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        this._reportersService
          .update(
            this.formReporter.get("id").value,
            this.getDirtyValues(this.formReporter)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadReporters(this.pagination);
          });
      } else if (closeValues.type === "delete") {
        this._reportersService
          .delete(this.formReporter.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadReporters(this.pagination);
          });
      }
    }

    this.formReporter.reset();
    this.formReporter.get("status").setValue("ACTIVE");
    this.formReporter.get("email").enable();

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  paginate(pagination: PaginationOptions) {
    this.loadReporters(pagination);
  }

  search() {
    this._reportersService
      .searchAll(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.reporters = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
