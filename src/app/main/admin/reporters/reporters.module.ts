import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { ModalModule } from "app/layout/components/modal/modal.module";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { ReportersComponent } from "./reporters.component";
import { ReportersService } from "./reporters.service";
import { UtilsModule } from "app/utils/utils.module";

@NgModule({
  declarations: [ReportersComponent],
  imports: [
    CoreCommonModule,
    NgbModule,
    ContentHeaderModule,
    ModalModule,
    PaginationModule,
    UtilsModule,
  ],
  providers: [ReportersService],
})
export class ReportersModule {}
