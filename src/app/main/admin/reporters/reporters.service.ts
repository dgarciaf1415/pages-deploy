import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "environments/environment";
import {
  Reporter,
  CreateReporterDto,
  UpdateReporterDto,
} from "app/common/interfaces/reporter.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Injectable({
  providedIn: "root",
})
export class ReportersService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/reporters`;
  }

  getAll(pagination: PaginationOptions) {
    const url = `${this.apiUrl}?limit=${pagination.limit}&offset=${pagination.offset}`;
    return this.http.get<Reporter[]>(url);
  }

  getById(reporterId: number) {
    const url = `${this.apiUrl}/${reporterId}`;
    return this.http.get<Reporter>(url);
  }

  search(type: string, search: string) {
    const url = `${this.apiUrl}/search/${type}/${search}`;
    return this.http.get<Reporter>(url);
  }

  searchAll(pagination: PaginationOptions, search: string) {
    const url = `${this.apiUrl}/search?limit=${pagination.limit}&offset=${pagination.offset}&search=${search}`;
    return this.http.get<Reporter[]>(url);
  }

  create(body: CreateReporterDto) {
    const url = this.apiUrl;
    const payload = Object.assign({}, body);
    delete (payload as any).surveyType;
    return this.http.post(url, payload);
  }

  update(reporterId: number, body: UpdateReporterDto) {
    const url = `${this.apiUrl}/${reporterId}`;
    return this.http.put(url, body);
  }

  delete(reporterId: number) {
    const url = `${this.apiUrl}/${reporterId}`;
    return this.http.put(url, { status: "DELETED" });
  }
}
