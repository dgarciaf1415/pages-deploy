import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { ActivitiesComponent } from "./activities.component";
import { ActivitiesService } from "./activities.service";

@NgModule({
  declarations: [ActivitiesComponent],
  imports: [CoreCommonModule, ContentHeaderModule, PaginationModule],
  providers: [ActivitiesService],
})
export class ActivitiesModule {}
