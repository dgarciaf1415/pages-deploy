import { Component, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Activity } from "app/common/interfaces/activity.type";
import { ActivitiesService } from "./activities.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Component({
  selector: "app-activities",
  templateUrl: "./activities.component.html",
  styleUrls: ["./activities.component.scss"],
})
export class ActivitiesComponent implements OnInit {
  public activities: Activity[];
  public contentHeader: object;
  public pagination: PaginationOptions;
  public searchItem: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private _activitiesService: ActivitiesService) {
    this._unsubscribeAll = new Subject();
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Activities",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.loadActivities(this.pagination);
  }

  loadActivities(pagination: PaginationOptions) {
    this._activitiesService
      .getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.activities = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  search() {
    this._activitiesService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.activities = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  paginate(pagination: PaginationOptions) {
    this.loadActivities(pagination);
  }
}
