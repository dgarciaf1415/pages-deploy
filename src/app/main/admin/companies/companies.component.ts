import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ModalOptions, ModalClose } from "app/common/interfaces/modal.type";
import { Company, CompanyType } from "app/common/interfaces/company.type";
import { CompaniesService } from "./companies.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";
import { AuthenticationService } from "app/auth/service";

@Component({
  selector: "app-companies",
  templateUrl: "./companies.component.html",
  styleUrls: ["./companies.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CompaniesComponent implements OnInit {
  public allCompanies: Company[];
  public companyUser: number;
  public companyTypes: { value: string }[];
  public companyStatus: { value: string }[];
  public companies: Company[];
  public companiesFiltered: Company[];
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formCompany: FormGroup;
  public pagination: PaginationOptions;
  public searchItem: string;
  public companyName: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _companiesService: CompaniesService,
    private _modalService: ModalService,
    private _authenticationService: AuthenticationService
  ) {
    this._unsubscribeAll = new Subject();

    this.formCompany = this.fb.group({
      id: [],
      name: ["", Validators.required],
      type: ["", Validators.required],
      status: ["ACTIVE"],
      companies: [""],
    });
    this.companyTypes = [
      { value: "ADMIN" },
      { value: "AGENCY" },
      { value: "CLIENT" },
    ];
    this.companyStatus = [{ value: "ACTIVE" }, { value: "INACTIVE" }];
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
    this.companyUser = (this._authenticationService.currentUserValue as any).user.company.id;
    this.companyName = CompanyType;

    if (
      (this._authenticationService.currentUserValue as any).user.profile.id > 1
    ) {
      this.companyTypes = this.companyTypes.filter((t) => {
        return t.value !== "ADMIN";
      });
    }
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Accounts",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formCompany.valueChanges.subscribe(() => {
      if (this.formCompany.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadCompanies(this.pagination);
  }

  loadCompanies(pagination: PaginationOptions) {
    this._companiesService
      .getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.companies = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  allLoadCompanies() {
    this._companiesService
      .getAll()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.allCompanies = res.rows;
      });
  }

  modalOpen(type: string, companyId: number) {
    this._modalService.open(type);
    this.allLoadCompanies();
    if (companyId) {
      if (type === "edit") {
        this._companiesService
          .getById(companyId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((company) => {
            let companiesRelated = [];

            if (company.type === 'AGENCY') {
              companiesRelated = company.companies.map((company: any) => {
                return company.hasCompany;
              });
            } else if (company.type === 'CLIENT') {
              companiesRelated = company.hasCompanies.map((company: any) => {
                return company.company;
              });
            }

            this.formCompany.get("id").setValue(company.id);
            this.formCompany.get("name").setValue(company.name);
            this.formCompany.get("type").setValue(company.type);
            this.formCompany.get("status").setValue(company.status);
            this.formCompany.get("companies").setValue(companiesRelated);

            this.filterCompanies(company.type);
          });
        this._modalService.options(
          (this.displayModal = {
            title: "Edit account",
            buttonText: "Update",
            enableButton: false,
          })
        );
      } else {
        this.formCompany.get("id").setValue(companyId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete account",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "account",
            itemName: this.companies.find((company) => {
              return company.id === companyId;
            }).name,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create account",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        delete this.formCompany.value.id;
        this.formCompany.value.companies = this.formCompany.value.companies || null;
        this._companiesService
          .create(this.formCompany.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadCompanies(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        this._companiesService
          .update(
            this.formCompany.get("id").value,
            this.getDirtyValues(this.formCompany)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadCompanies(this.pagination);
          });
      } else if (closeValues.type === "delete") {
        this._companiesService
          .delete(this.formCompany.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadCompanies(this.pagination);
          });
      }
    }

    this.formCompany.reset();
    this.formCompany.get("type").setValue("");
    this.formCompany.get("companies").setValue("");
    this.formCompany.get("status").setValue("ACTIVE");

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  filterCompanies(companyType: string) {
    if (companyType === 'AGENCY') {
      this.companiesFiltered = this.allCompanies.filter((company) => {
        return company.type === 'CLIENT'
      });
    } else if (companyType === 'CLIENT') {
      this.companiesFiltered = this.allCompanies.filter((company) => {
        return company.type === 'AGENCY'
      });
    } else {
      this.companiesFiltered = this.allCompanies;
    }    
  }

  paginate(pagination: PaginationOptions) {
    this.loadCompanies(pagination);
  }

  search() {
    this._companiesService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.companies = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
