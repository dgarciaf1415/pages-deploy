import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { ModalModule } from "app/layout/components/modal/modal.module";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { CompaniesComponent } from "./companies.component";
import { CompaniesService } from "./companies.service";

@NgModule({
  declarations: [CompaniesComponent],
  imports: [
    CoreCommonModule,
    NgbModule,
    ContentHeaderModule,
    ModalModule,
    PaginationModule,
    NgSelectModule,
  ],
  providers: [CompaniesService],
})
export class CompaniesModule {}
