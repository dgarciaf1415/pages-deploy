import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { ModalModule } from "app/layout/components/modal/modal.module";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { ProfilesComponent } from "./profiles.component";
import { ProfilesService } from "./profiles.service";

@NgModule({
  declarations: [ProfilesComponent],
  imports: [
    CoreCommonModule,
    NgbModule,
    ContentHeaderModule,
    ModalModule,
    PaginationModule,
  ],
  providers: [ProfilesService],
})
export class ProfilesModule {}
