import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "environments/environment";
import {
  CreateProfileDto,
  Profile,
  UpdateProfileDto,
} from "app/common/interfaces/profile.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Injectable({
  providedIn: "root",
})
export class ProfilesService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/profiles`;
  }

  getAll(pagination?: PaginationOptions) {
    const url = pagination ? `${this.apiUrl}?limit=${pagination.limit}&offset=${pagination.offset}` : this.apiUrl;
    return this.http.get<Profile[]>(url);
  }

  getById(profileId: number) {
    const url = `${this.apiUrl}/${profileId}`;
    return this.http.get<Profile>(url);
  }

  search(pagination: PaginationOptions, search: string) {
    const url = `${this.apiUrl}/search?limit=${pagination.limit}&offset=${pagination.offset}&search=${search}`;
    return this.http.get<Profile[]>(url);
  }

  create(body: CreateProfileDto) {
    const url = this.apiUrl;
    return this.http.post(url, body);
  }

  update(profileId: number, body: UpdateProfileDto) {
    const url = `${this.apiUrl}/${profileId}`;
    return this.http.put(url, body);
  }

  delete(profileId: number) {
    const url = `${this.apiUrl}/${profileId}`;
    return this.http.put(url, { status: "DELETED" });
  }
}
