import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ModalOptions, ModalClose } from "app/common/interfaces/modal.type";
import { Profile, ProfileType } from "app/common/interfaces/profile.type";
import { ProfilesService } from "./profiles.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { PaginationOptions } from "app/common/interfaces/pagination.type";

@Component({
  selector: "app-profiles",
  templateUrl: "./profiles.component.html",
  styleUrls: ["./profiles.component.scss"],
})
export class ProfilesComponent implements OnInit {
  public contentHeader: object;
  public displayModal: ModalOptions;
  public formProfile: FormGroup;
  public pagination: PaginationOptions;
  public profiles: Profile[];
  public profileStatus: { value: string }[];
  public searchItem: string;
  public profileTypes: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private _profilesService: ProfilesService,
    private _modalService: ModalService
  ) {
    this._unsubscribeAll = new Subject();

    this.formProfile = this.fb.group({
      id: [],
      name: ["", [Validators.required]],
      status: ["ACTIVE"],
    });
    this.profileStatus = [{ value: "ACTIVE" }, { value: "INACTIVE" }];
    this.pagination = {
      limit: 8,
      offset: 0,
      total: 0,
    };
    this.profileTypes = ProfileType;
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Profiles",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formProfile.valueChanges.subscribe(() => {
      if (this.formProfile.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.loadProfiles(this.pagination);
  }

  loadProfiles(pagination: PaginationOptions) {
    this._profilesService.getAll(pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.profiles = res.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: res.count,
        };
      });
  }

  modalOpen(type: string, profileId: number) {
    this._modalService.open(type);
    if (profileId) {
      if (type === "edit") {
        this._profilesService
          .getById(profileId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((profile) => {
            this.formProfile.get("id").setValue(profile.id);
            this.formProfile.get("name").setValue(profile.name);
            this.formProfile.get("status").setValue(profile.status);
          });
        this._modalService.options(
          (this.displayModal = {
            title: "Edit profile",
            buttonText: "Update",
            enableButton: false,
          })
        );
      } else {
        this.formProfile.get("id").setValue(profileId);
        this._modalService.options(
          (this.displayModal = {
            title: "Delete profile",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "profile",
            itemName: this.profiles.find((profile) => {
              return profile.id === profileId;
            }).name,
          })
        );
      }
    } else {
      this._modalService.options(
        (this.displayModal = {
          title: "Create profile",
          buttonText: "Create",
          enableButton: false,
        })
      );
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        delete this.formProfile.value.id;
        this._profilesService
          .create(this.formProfile.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadProfiles(this.pagination);
          });
      } else if (closeValues.type === "edit") {
        this._profilesService
          .update(
            this.formProfile.get("id").value,
            this.getDirtyValues(this.formProfile)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadProfiles(this.pagination);
          });
      } else if (closeValues.type === "delete") {
        this._profilesService
          .delete(this.formProfile.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadProfiles(this.pagination);
          });
      }
    }

    this.formProfile.reset();
    this.formProfile.get("status").setValue("ACTIVE");

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  paginate(pagination: PaginationOptions) {
    this.loadProfiles(pagination);
  }

  search() {
    this._profilesService
      .search(this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.profiles = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else dirtyValues[key] = currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
