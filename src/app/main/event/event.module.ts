import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { ProjectsComponent } from "./projects/projects.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { EventSinglesComponent } from "./event-singles/event-singles.component";
import { EventGroupsComponent } from "./event-groups/event-groups.component";
import { ProjectsModule } from "./projects/projects.module";
import { EventGroupsModule } from "./event-groups/event-groups.module";
import { EventSinglesModule } from "./event-singles/event-singles.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { InviteComponent } from "./dashboard/invite/invite.component";
import { NoAuthGuard } from "app/auth/helpers/no-auth.guards";
import { AuthGuard } from "app/auth/helpers";
import { UtilsModule } from "app/utils/utils.module";

const routes: Routes = [
	{
		path: "",
		redirectTo: "projects",
		pathMatch: "full",
	},
	{
		path: "projects",
		component: ProjectsComponent,
		data: { animation: "misc" },
		canActivate: [AuthGuard],
	},
	{
		path: "dashboard/:id",
		component: DashboardComponent,
		data: { animation: "misc" },
		canActivate: [AuthGuard],
	},
	{
		path: "dashboard/:id/reporters",
		component: InviteComponent,
		data: { animation: "misc" },
		canActivate: [AuthGuard],
	},
	{
		path: "report/:id",
		component: DashboardComponent,
		canActivate: [NoAuthGuard],
		data: { animation: "misc" },
	},
	{
		path: "groups/:id",
		component: EventGroupsComponent,
		data: { animation: "misc" },
		canActivate: [AuthGuard],
	},
	{
		path: "single/:id",
		component: EventSinglesComponent,
		data: { animation: "misc" },
		canActivate: [AuthGuard],
	},
];

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		ProjectsModule,
		EventGroupsModule,
		EventSinglesModule,
		DashboardModule,
		UtilsModule,
	],
})
export class EventModule {}
