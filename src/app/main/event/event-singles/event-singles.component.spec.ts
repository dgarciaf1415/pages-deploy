import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSinglesComponent } from './event-singles.component';

describe('EventSinglesComponent', () => {
  let component: EventSinglesComponent;
  let fixture: ComponentFixture<EventSinglesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventSinglesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventSinglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
