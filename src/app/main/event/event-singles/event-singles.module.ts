import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreCommonModule } from '@core/common.module';
import { RouterModule } from '@angular/router';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { CoreDirectivesModule } from '@core/directives/directives';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NouisliderModule } from 'ng2-nouislider';
import { NgxMaskModule } from 'ngx-mask';

import { UtilsModule } from 'app/utils/utils.module';
import { ModalModule } from 'app/layout/components/modal/modal.module';
import { EventSinglesComponent } from './event-singles.component';
import { NgbDateCustomParserFormatter } from 'app/common/services/ngb-date-custom-parser-formatter';


@NgModule({
  declarations: [
    EventSinglesComponent
  ],
  imports: [
    CommonModule,
    CoreCommonModule,
    NgbModule,
    NgxMaskModule.forRoot(),
    ModalModule,
    CoreDirectivesModule,
    NouisliderModule,
    AutocompleteLibModule,
    UtilsModule,
    RouterModule
  ],
  providers: [
    { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter }
  ],
})
export class EventSinglesModule { }
