import { TestBed } from '@angular/core/testing';

import { EventSinglesService } from './event-singles.service';

describe('EventSinglesService', () => {
  let service: EventSinglesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventSinglesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
