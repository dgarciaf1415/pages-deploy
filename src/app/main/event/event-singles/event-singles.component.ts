import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

import { environment } from "environments/environment";
import { File } from "app/common/interfaces/file.type";
import { ToastService } from "app/layout/components/toast/toast.service";
import { EventSinglesService } from "./event-singles.service";
import { EventDataService } from "../event-groups/event-data.service";
import { ModalClose, ModalOptions } from "app/common/interfaces/modal.type";
import { ModalService } from "app/layout/components/modal/modal.service";
import { Validations } from "app/utils/validations/validations";

@Component({
  selector: "app-event-singles",
  templateUrl: "./event-singles.component.html",
  styleUrls: ["./event-singles.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class EventSinglesComponent implements OnInit {
  public ageRange: object[];
  public assetQuantity: number;
  public assetQuantityArray: number[];
  public configSliderAge: object;
  public displayModal: ModalOptions;
  public editEvent: boolean;
  public event: any;
  public eventAssetTypes: any;
  public eventAssetTypesSelected: number[];
  public eventCalendarStartDate: NgbDateStruct;
  public eventCalendarEndDate: NgbDateStruct;
  public dataForEvent: boolean;
  public formEvent: FormGroup;
  public formHasErrors: boolean[];
  public imageContainer: string;
  public maxAssets: number;
  public maxTargets: number;
  public targetQuantity: number;
  public targetQuantityArray: number[];

  public $assetObjetives: Observable<any>;
  public $eventObjetives: Observable<any>;
  public $eventGenders: Observable<any>;
  public $eventEthnicities: Observable<any>;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private _router: Router,
    private _eventsService: EventSinglesService,
    private _eventDataService: EventDataService,
    private _toastService: ToastService,
    private _modalService: ModalService,
  ) {
    this.ageRange = [];
    this.assetQuantity = 0;
    this.assetQuantityArray = [];
    this.configSliderAge = {
      behaviour: "drag",
      connect: true,
      margin: 3,
      limit: 70,
      tooltips: true,
      step: 1,
      range: {
        min: 0,
        max: 85,
      },
      pips: {
        mode: "range",
        density: 5,
      },
    };
    this.editEvent = false;
    this.imageContainer = environment.spaceUrl;
    this.maxAssets = 40;
    this.maxTargets = 2;
    this.targetQuantity = 0;
    this.targetQuantityArray = [];
    this._unsubscribeAll = new Subject();

    if (this.route.snapshot.paramMap.get("id")) {
      const eventId = parseInt(this.route.snapshot.paramMap.get("id"));
      this.loadEvent(eventId);
    }

    this.formEvent = this.fb.group({
      id: [],
      name: ["", Validators.required],
      location: ["", Validators.required],
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      startTime: [""],
      endTime: [""],
      budget: ["", Validators.required],
      sponsorshipInvestment: ["", Validators.required],
      attendance: ["", Validators.required],
      eventObjetive: ["", Validators.required],
      eventSecondaryObjetive: [""],
      targets: this.fb.group({}),
      assets: this.fb.group({}),
      status: ["ACTIVE"],
      eventGroup: [""],
    }, {
      validator: Validations.compareTimes('startTime', 'endTime')
    });

    this.formHasErrors = [];
  }

  ngOnInit(): void {}

  loadEvent(eventId: number) {
    this._eventsService
      .getById(eventId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res) => {
        this.event = res;

        this.formEvent.get("id").setValue(this.event.id);
        this.formEvent.get("name").setValue(this.event.name);
        this.formEvent.get("location").setValue(this.event.location);
        this.formEvent
          .get("startDate")
          .setValue(this.formatDate(this.event.startDate));
        this.formEvent
          .get("endDate")
          .setValue(this.formatDate(this.event.endDate));
        this.formEvent
          .get("startTime")
          .setValue(this.formatTime(this.event.startDate));
        this.formEvent
          .get("endTime")
          .setValue(this.formatTime(this.event.endDate));
        this.formEvent.get("budget").setValue(this.event.budget);
        this.formEvent.get("sponsorshipInvestment").setValue(this.event.sponsorshipInvestment);
        this.formEvent.get("attendance").setValue(this.event.attendance);
        this.formEvent
          .get("eventObjetive")
          .setValue(this.event.eventObjetive.id);
        this.formEvent
          .get("eventSecondaryObjetive")
          .setValue(
            this.event.eventSecondaryObjetive
              ? this.event.eventSecondaryObjetive.id
              : ""
          );
        this.formEvent.get("eventGroup").setValue(this.event.eventGroup.id);

        for (const target of this.event.targets) {
          this.addNewTarget(target);
        }

        for (const asset of this.event.assets) {
          this.addNewAsset(asset);
        }

        this.loadDataforEvent();

        this.formEvent.disable();
      });
  }

  loadDataforEvent() {
    const eventType = this.event.eventGroup.eventType;

    this.$eventGenders = this._eventDataService.getEventGenders();
    this.$eventEthnicities = this._eventDataService.getEventEthnicities();
    this.$assetObjetives = this._eventDataService.getAssetObjetives();

    if (eventType) {
      this.dataForEvent = true;
      this.$eventObjetives =
        this._eventDataService.getEventObjetivesByEventType(eventType.id);
      this._eventDataService.getAssetTypes().subscribe((res) => {
          this.eventAssetTypes = res;
        });
    } else {
      this.dataForEvent = false;
      this.$eventObjetives =
        this._eventDataService.getEventObjetivesByEventType(0);
      this._eventDataService.getAssetTypes().subscribe((res) => {
        this.eventAssetTypes = res;
      });
    }
  }

  searchName(elem: any) {
    if (elem.target.value) {
      this._eventsService
        .searchByType("name", elem.target.value)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((res) => {
          if (res) {
            this.formEvent.get("name").setErrors({ nameExist: true });
          } else {
            this.formEvent.get("name").setErrors(null);
          }
        });
    }
  }

  addNewTarget(data?: any) {
    if (this.targetQuantityArray.length < this.maxTargets) {
      const targets = this.formEvent.get("targets") as FormGroup;
      let id = null;
      let name = "";
      let expectedAttendance = "";
      let eventAgeMin = 5;
      let eventAgeMax = 30;
      let eventGender = "";
      let eventEthnicity = "";

      if (data) {
        id = data.id;
        (name = data.name), (expectedAttendance = data.expectedAttendance);
        eventAgeMin = data.eventAgeMin;
        eventAgeMax = data.eventAgeMax;
        eventGender = data.eventGender.id;
        eventEthnicity = data.eventEthnicity.id;
      }

      this.targetQuantity++;
      this.targetQuantityArray.push(this.targetQuantity);
      this.ageRange.push({
        id: this.targetQuantity,
        range: [eventAgeMin, eventAgeMax],
      });

      targets.setControl(
        `target_${this.targetQuantity}`,
        this.fb.group({
          id: [id],
          name: [name, Validators.required],
          expectedAttendance: [expectedAttendance, Validators.required],
          eventAgeMin: [eventAgeMin, Validators.required],
          eventAgeMax: [eventAgeMax, Validators.required],
          eventGender: [eventGender, Validators.required],
          eventEthnicity: [eventEthnicity, Validators.required],
        })
      );
    }
  }

  addNewAsset(data?: any) {
    if (this.assetQuantityArray.length < this.maxAssets) {
      const assets = this.formEvent.get("assets") as FormGroup;
      let id = null;
      let name = "";
      let type = "";
      let objetive = null;
      let expectedQuantity = 1;
      let description = "";

      if (data) {
        id = data.id;
        name = data.name;
        type = data.assetType.id;
        objetive = data.assetObjetive ? data.assetObjetive.id : '';
        expectedQuantity = data.expectedQuantity;
        description = data.description;
      }

      this.assetQuantity++;
      this.assetQuantityArray.push(this.assetQuantity);

      assets.setControl(
        `asset_${this.assetQuantity}`,
        this.fb.group({
          id: [id],
          nameRaw: ['', Validators.required],
          name: [name, Validators.required],
          type: [type, Validators.required],
          objetive: [objetive, Validators.required],
          expectedQuantity: [expectedQuantity, Validators.required],
          description: [description],
        })
      );
    }
  }

  deleteTarget(target: number) {
    const targets = this.formEvent.get("targets") as FormGroup;

    for (let i = 0; i < this.ageRange.length; i++) {
      if ((this.ageRange[i] as any).id === target) {
        this.ageRange.splice(i, 1);
        this.targetQuantityArray.splice(i, 1);
        break;
      }
    }

    targets.removeControl(`target_${target}`);
  }

  deleteAsset(asset: number) {
    const assets = this.formEvent.get("assets") as FormGroup;

    for (let i = 0; i < this.assetQuantityArray.length; i++) {
      if (this.assetQuantityArray[i] === asset) {
        this.assetQuantityArray.splice(i, 1);
        break;
      }
    }

    assets.removeControl(`asset_${asset}`);
  }

  toogleEdit(elem: PointerEvent, type?: string) {
    const button: any = elem.target;

    if (type === "cancel") {
      this.editEvent = false;
      this.formEvent.disable();
      button.innerHTML = "<span>Edit</span>";
      this.loadEvent(this.formEvent.get("id").value);
    } else {
      if (this.editEvent) {
        if (this.eachFieldIsValid(this.formEvent).includes(false)) {
          this._toastService.show('There are mandatory fields, check the form and try again.', {
            autohide: true,
            headerTitle: "Error",
            icon: 'alert-circle',
            iconColorClass: "text-danger",
          });

          this.formHasErrors = [];
          return;
        }

        button.innerHTML =
          '<span><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>&nbsp; Update</span>';
        this.editEvent = false;
        this.formEvent.disable();

        const dataTouched: any = this.formEvent.value;
        const payload: any = { ...dataTouched };
        const onlyDataModified: any = this.getDirtyValues(this.formEvent);

        if (onlyDataModified.hasOwnProperty('startDate')) {
          onlyDataModified.startDate = this.dateFormat(payload.startDate, payload?.startTime, "start");
          delete onlyDataModified.startTime;
        }

        if (onlyDataModified.hasOwnProperty('endDate')) {
          onlyDataModified.endDate = this.dateFormat(payload.endDate, payload?.endTime, "end");
          delete onlyDataModified.endTime;
        }

        if (onlyDataModified.hasOwnProperty('startTime')) {
          onlyDataModified.startDate = this.dateFormat(payload.startDate, payload.startTime, "start");
          delete onlyDataModified.startTime;
        }

        if (onlyDataModified.hasOwnProperty('endTime')) {
          onlyDataModified.endDate = this.dateFormat(payload.endDate, payload.endTime, "end");
          delete onlyDataModified.endTime;
        }
        
        if (onlyDataModified.targets) {
          payload.targets = [];
          for (const key in dataTouched.targets) {
            if (
              Object.prototype.hasOwnProperty.call(dataTouched.targets, key)
            ) {
              payload.targets.push(dataTouched.targets[key]);
            }
          }

          onlyDataModified.targets = payload.targets;
        }

        if (onlyDataModified.assets) {
          payload.assets = [];
          for (const key in dataTouched.assets) {
            if (Object.prototype.hasOwnProperty.call(dataTouched.assets, key)) {
              delete dataTouched.assets[key].nameRaw;
              payload.assets.push(dataTouched.assets[key]);
            }
          }

          onlyDataModified.assets = payload.assets;
        }
      
        this._eventsService
          .update(this.formEvent.get("id").value, onlyDataModified)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            button.innerHTML = "<span>Edit</span>";
            this.formEvent.disable();
            const savedText = "Event was updated successfully";
            this._toastService.show(savedText, {
              autohide: true,
              headerTitle: "Success",
              icon: "check",
              iconColorClass: "text-success",
            });
          });
      } else {
        button.innerHTML = "<span>Update</span>";
        this.editEvent = true;
        this.formEvent.enable();
      }
    }
  }

  modalOpen(type: string, eventId: number) {
    this._modalService.open(type);
    if (eventId) {
      if (type === "delete") {
        this._modalService.options(
          (this.displayModal = {
            title: "Delete event",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "group",
            itemName: this.event.name,
          })
        );
      }
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "delete") {
        this._eventsService
          .delete(this.formEvent.get("id").value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            const savedText = "Event was deleted successfully";
            this._toastService.show(savedText, {
              autohide: true,
              headerTitle: "Success",
              icon: "check",
              iconColorClass: "text-success",
            });
            this._router.navigate(["/"]);
          });
      }
    }

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  validatePercentage(elem: InputEvent, targetId: number) {
    const field: any = elem.target;
    const formField = this.formEvent.get(
      `targets.target_${targetId}.expectedAttendance`
    );
    let otherFormField;

    if (this.targetQuantityArray.length > 1) {
      if (this.targetQuantityArray[0] === targetId) {
        otherFormField = this.formEvent.get(
          `targets.target_${this.targetQuantityArray[1]}.expectedAttendance`
        );
      } else {
        otherFormField = this.formEvent.get(
          `targets.target_${this.targetQuantityArray[0]}.expectedAttendance`
        );
      }
    }

    if (field.value > 100) {
      field.value = 100;
      return formField.patchValue(100);
    }

    if (!!field.value && !!otherFormField?.value) {
      if (parseInt(field.value) + parseInt(otherFormField.value) > 100) {
        const newValue = 100 - parseInt(otherFormField.value);
        field.value = newValue;
        return formField.patchValue(newValue);
      }
    }
  }

  async uploadFile(file: any, fieldName: string) {
    const fileData = new FormData();
    const fileLoaded = file.target.files[0];
    const assets = this.formEvent.get("assets") as FormGroup;

    fileData.append("image", fileLoaded, fileLoaded.name);

    if (fileLoaded.size > 10000000) {
      this._toastService.show('The file uploaded exceed weight parameters', {
        autohide: true,
        headerTitle: "Error",
        icon: 'alert-circle',
        iconColorClass: "text-danger",
      });

      assets.get(`${fieldName}.nameRaw`).reset();
      assets.get(`${fieldName}.name`).reset();
      return;
    }

    if (fileLoaded) {
      await this._eventsService.upload(fileData).subscribe((f: File) => {
        assets.get(`${fieldName}.name`).setValue(f.filename);
      });
    }
  }

  setValues(fieldName: string, values: number[]) {
    const targets = this.formEvent.get("targets") as FormGroup;
    targets.get(`${fieldName}.eventAgeMin`).setValue(values[0]);
    targets.get(`${fieldName}.eventAgeMax`).setValue(values[1]);
  }

  formatDate(date: string) {
    const onlyDate = date.split('T');
    const parseDate = onlyDate[0].split('-');

    return {
      year: Number(parseDate[0]),
      month: Number(parseDate[1]),
      day: Number(parseDate[2]),
    };
  }

  formatTime(date: string) {
    const onlyTime = date.split('T');
    const parseDate = onlyTime[1].split(':');

    return {
      hour: Number(parseDate[0]),
      minute: Number(parseDate[1]),
    };
  }

  dateFormat(value: any, timeRaw: any, type: string) {
    if (value) {
      const month =
        value.month.toString().length > 1 ? value.month : `0${value.month}`;
      const day = value.day.toString().length > 1 ? value.day : `0${value.day}`;
      let time = type === "start" ? "00:00:00" : "23:59:59";

      if (timeRaw) {
        time = this.timeFormat(timeRaw);
      }

      return `${value.year}-${month}-${day} ${time}`;
    }
  }

  timeFormat(value: any) {
    if (value) {
      const hour = value.hour.toString().length > 1 ? value.hour : `0${value.hour}`;
      const minute = value.minute.toString().length > 1 ? value.minute : `0${value.minute}`;

      return `${hour}:${minute}:00`;
    }
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};
    let dateFields = ["startDate", "endDate"];

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls) {
          dirtyValues[key] = this.getDirtyValues(currentControl);
        } else {
          dirtyValues[key] = currentControl.value;
        }
      }
    });

    return dirtyValues;
  }

  eachFieldIsValid(form: any) {
    let dateFields = ["startDate", "endDate", "nameRaw"];

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl) {
        if (currentControl.controls) {
          return this.eachFieldIsValid(currentControl);
        } else {
          if (!dateFields.includes(key)) {
            this.formHasErrors.push(currentControl.valid);
          }
        }
      }
    });
  
    return this.formHasErrors;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
