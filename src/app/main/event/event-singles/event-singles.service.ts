import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { environment } from "environments/environment";
import { CreateEventDto, Event, UpdateEventDto } from "app/common/interfaces/event-single.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";
import { ToastService } from "app/layout/components/toast/toast.service";

@Injectable({
  providedIn: "root",
})
export class EventSinglesService {
  apiUrl: string;

  constructor(private http: HttpClient, private _toastService: ToastService) {
    this.apiUrl = `${environment.apiUrl}/events`;
  }

  getAllByGroup(groupId: number, pagination: PaginationOptions) {
    const url = `${this.apiUrl}/group/${groupId}?limit=${pagination.limit}&offset=${pagination.offset}`;
    return this.http.get<Event[]>(url);
  }

  getById(eventId: number) {
    const url = `${this.apiUrl}/${eventId}`;
    return this.http.get<Event>(url);
  }

  create(body: CreateEventDto) {
    const url = this.apiUrl;
    return this.http.post(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  searchByType(type: string, search: string) {
    const url = `${this.apiUrl}/search/${type}/${search}`;
    return this.http.get<Event>(url);
  }

  upload(file: any) {
    const url = `${this.apiUrl}/images`;
    return this.http.post(url, file).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  duplicate(eventId: number) {
    const url = `${this.apiUrl}/copy/${eventId}`;
    return this.http.get(url);
  }

  updateSurveyType(eventReporterId: number, survey: number) {
    const url = `${this.apiUrl}/survey-type/${eventReporterId}`;
    const body = {
      survey,
    }
    return this.http.put(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  update(eventId: number, body: UpdateEventDto) {
    const url = `${this.apiUrl}/${eventId}`;
    return this.http.put(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  delete(eventId: number) {
    const url = `${this.apiUrl}/${eventId}`;
    return this.http.put(url, { status: "DELETED" }).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  assignReporter(eventId: number, reporterId: number, surveyId: number) {
    const url = `${this.apiUrl}/reporter`;
    const body = {
      event: eventId,
      reporter: reporterId,
      survey: surveyId,
    };
    return this.http.post(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  unassignReporter(eventId: number, reporterId: number) {
    const url = `${this.apiUrl}/reporter/${reporterId}/event/${eventId}`;
    return this.http.put(url, {}).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  resendEmailToReporter(reporterId: number, eventId: number) {
    const url = `${this.apiUrl}/resend/reporter/${reporterId}/event/${eventId}`;
    return this.http.get(url).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  getReporters(eventId: number) {
    const url = `${this.apiUrl}/reporters/${eventId}`;
    return this.http.get(url);
  }
}
