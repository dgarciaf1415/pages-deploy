import { Component, Input, OnInit } from "@angular/core";

@Component({
	selector: "app-dashboard-description",
	templateUrl: "./dashboard-description.component.html",
	styleUrls: ["./dashboard-description.component.scss"],
})
export class DashboardDescriptionComponent implements OnInit {
	@Input() group: string;
	@Input() budget: number;
	@Input() objetive: String;
	@Input() eventName: string
	@Input() eventType: String;
	@Input() eventClient: string;
	@Input() eventAgency: string;
	@Input() eventCreatedBy: string;
	@Input() location: String;
	@Input() dateStart: Date;
	@Input() dateFinish: Date;
	@Input() targetAudience: string;
	@Input() eventBudget: string;
	@Input() eventInvestment: string;
	@Input() eventRatio: string;
  @Input() invited: number;
	@Input() reported: number;

	public isMenuToggled = false;
  public tooltipTexts: any;


	constructor() {
    this.tooltipTexts = {
      overviewReporters: 'Reporters are the stakeholders who have been assigned to validate the event\'s KPIs',
    }
  }

	ngOnInit(): void {}
}
