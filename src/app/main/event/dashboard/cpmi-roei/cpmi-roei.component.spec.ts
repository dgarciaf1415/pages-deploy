import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CpmiRoeiComponent } from './cpmi-roei.component';

describe('CpmiRoeiComponent', () => {
  let component: CpmiRoeiComponent;
  let fixture: ComponentFixture<CpmiRoeiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CpmiRoeiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CpmiRoeiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
