import { Component, Input, OnInit } from '@angular/core';

import { environment } from 'environments/environment';

@Component({
  selector: 'app-cpmi-roei',
  templateUrl: './cpmi-roei.component.html',
  styleUrls: ['./cpmi-roei.component.scss']
})
export class CpmiRoeiComponent implements OnInit {
  @Input() projectImage: string;
  @Input() cpmi: number;
  @Input() roei: number;

  public cpmiValue: number;
  public imageContainer: string;
  public roeiValue: number | string;

  constructor() {
    this.imageContainer = environment.spaceUrl;
  }

  ngOnInit(): void {
    this.cpmiValue = this.cpmi ? Number(this.cpmi) : 0;
    this.roeiValue = this.roei.toFixed(2).replace(/\.00$/,'');
  }

}
