import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";

import { environment } from "environments/environment";
import { ToastService } from "app/layout/components/toast/toast.service";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  apiUrl: string;

  constructor(private http: HttpClient, private _toastService: ToastService) {
    this.apiUrl = `${environment.apiUrl}/events`;
  }

  getByUrl(url: string) {
    return this.http.get(`${this.apiUrl}/report/by/url/${url}`).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: "Error",
          icon: "alert-circle",
          iconColorClass: "text-danger",
        });

        return throwError(error);
      })
    );
  }

  getById(id: number) {
    return this.http.get(`${this.apiUrl}/report/by/id/${id}`).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: "Error",
          icon: "alert-circle",
          iconColorClass: "text-danger",
        });

        return throwError(error);
      })
    );
  }
}
