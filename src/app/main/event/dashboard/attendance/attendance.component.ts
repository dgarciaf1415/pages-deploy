import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApexChart, ApexPlotOptions, ApexStroke, ApexDataLabels, ApexFill, ApexLegend, ApexMarkers, ApexNonAxisChartSeries, ApexResponsive, ApexStates, ApexTooltip, ApexXAxis, ApexYAxis } from "ng-apexcharts";

export interface ChartOptions {
  // Apex-non-axis-chart-series
  series?: ApexNonAxisChartSeries;
  chart?: ApexChart;
  stroke?: ApexStroke;
  tooltip?: ApexTooltip;
  dataLabels?: ApexDataLabels;
  fill?: ApexFill;
  colors?: string[];
  legend?: ApexLegend;
  labels?: any;
  plotOptions?: ApexPlotOptions;
  responsive?: ApexResponsive[];
  markers?: ApexMarkers[];
  xaxis?: ApexXAxis;
  yaxis?: ApexYAxis;
  states?: ApexStates;
}

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {
  @ViewChild('apexDonutChartRef') apexDonutChartRef: any;
  
	@Input() attendanceActuals: number;
  @Input() attendanceExpected: number;
  @Input() attendancePercentage: number;
  @Input() attendanceToDate: string;
  @Input() dateStart: Date;
  @Input() eventName: string
  @Input() targets: any[];

  public apexDonutChart: Partial<ChartOptions>;
  public attendancePercentageValue: number | string;
  public toDateValue: any;
  public tooltipTexts: any;

  // Color Variables
  public chartColors = {
    donut: {
      series1: '#60CB68',
      series2: '#D0F9D3',
    },
  };

  constructor() { }

  ngOnInit(): void {
    const targetPercentage = this.targets && this.targets.length > 0 ? this.targets[0].attendance : 0;
    this.attendancePercentageValue = this.attendancePercentage.toFixed(2).replace(/\.00$/,'');

    this.toDateValue = {
      value1: this.attendanceToDate[0],
      value2: (this.attendanceToDate[1] as any)?.toFixed(2).replace(/\.00$/,'')
    };

    this.apexDonutChart = {
      series: [parseFloat(targetPercentage.toFixed(2)), (100 - parseFloat(targetPercentage.toFixed(2)))],
      chart: {
        height: 230,
        type: 'donut',
				width: 220,
      },
      colors: [
        this.chartColors.donut.series1,
        this.chartColors.donut.series2,
      ],
			dataLabels: {
				enabled: false,
			},
      plotOptions: {
        pie: {
          donut: {
            size: '72',
            labels: {
              show: true,
              name: {
                offsetY: 20,
                fontSize: '1rem',
                fontFamily: 'Roboto'
              },
              value: {
                fontSize: '2.4rem',
								offsetY: -10,
                fontFamily: 'Roboto',
                formatter: function (val) {
                  return parseFloat(val) + '%';
                }
              },
              total: {
                show: true,
								color: '#000',
                fontSize: '0.62rem',
                label: this.targets[0].name,
                formatter: function (w) {
                  return `${targetPercentage}%`;
                }
              }
            }
          }
        }
      },
      legend: {
        show: false,
        position: 'bottom'
      },
      labels: [this.targets.length > 0 ? this.targets[0].name : 0, ''],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              height: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }
      ]
    };
  }

}
