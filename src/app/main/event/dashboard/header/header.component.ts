import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from "@angular/core";

import { ToastService } from "app/layout/components/toast/toast.service";
import { environment } from "environments/environment";

@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit {
	@Output() saveReportToPdf = new EventEmitter<string>();

	@Input() projectImage: String;
	@Input() projectTitle: String;
	@Input() eventGroupTitle: String;
	@Input() eventClient: String;
	@Input() contReporters: Number;
	@Input() eventReportUrl: string;
	@Input() isPublic: boolean;
	@Input() eventId: number;
	@Input() enableSavingTemporary: boolean;

	public webUrl: string;
	public imageContainer: string;

	constructor(private _toastService: ToastService) {
		this.webUrl = environment.adminUrl;
		this.imageContainer = environment.spaceUrl;
	}

	ngOnInit(): void {}

	saveToPdf() {
		this.saveReportToPdf.emit(this.eventReportUrl);
	}

	copyToClipboard() {
		const reportUrl = `${this.webUrl}/event/report/${this.eventReportUrl}`;
		const selectBox = document.createElement('textarea');
		
    selectBox.style.position = 'fixed';
    selectBox.value = reportUrl;
    document.body.appendChild(selectBox);
    selectBox.focus();
    selectBox.select();
    document.execCommand('copy');
    document.body.removeChild(selectBox);
	
		/* Copy the text inside the text field */
		// navigator.clipboard.writeText(reportUrl);
	
		/* Alert the copied text */
		this._toastService.show('URL copied', {
			autohide: true,
			headerTitle: "Success",
			icon: "check",
			iconColorClass: "text-success",
		});
	}

	reloadData() {
		localStorage.removeItem(`dashboard_data_${this.eventId}`);
		window.location.reload();
	}
}
