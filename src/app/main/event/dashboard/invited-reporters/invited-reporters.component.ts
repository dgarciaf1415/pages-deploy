import { Component, Input, OnInit, ViewChild } from "@angular/core";
import {
	ApexAxisChartSeries,
	ApexChart,
	ApexNonAxisChartSeries,
	ApexPlotOptions,
	ApexStroke,
	ChartComponent,
} from "ng-apexcharts";

@Component({
	selector: "app-invited-reporters",
	templateUrl: "./invited-reporters.component.html",
	styleUrls: ["./invited-reporters.component.scss"],
})
export class InvitedReportersComponent implements OnInit {
	@ViewChild("chart") chart: ChartComponent;
	@Input() chartOpt: ApexChart;
	@Input() series: ApexAxisChartSeries | ApexNonAxisChartSeries;
	@Input() colors: string[];
	@Input() plotOptions: ApexPlotOptions;
	@Input() stroke: ApexStroke;
	@Input() labels: string[];
	@Input() invited: number;
	@Input() reported: number;

	public unreporteredValue: string | number;
	public reportedValue: string | number;
	public reportedVsUnreportered: string;

	constructor() {}

	ngOnInit(): void {
		const percent = Number(parseFloat((+(this.reported * 100) / this.invited).toFixed(2))) ? parseFloat((+(this.reported * 100) / this.invited).toFixed(2)) : 0 

		this.unreporteredValue = this.invited ? (100 - ((this.reported / this.invited) * 100)).toFixed(2).replace(/\.00$/,'') : 0
		this.reportedValue = this.invited ? ((this.reported / this.invited) * 100).toFixed(2).replace(/\.00$/,'') : 0
		

		const reporters = this.reported
		const totalReporters = this.invited

		this.reportedVsUnreportered = totalReporters !== 0 ? `${reporters} reporters answered and ${totalReporters - reporters} give not answer` : '';
				
		this.chartOpt = {
			height: "150px",
			type: "radialBar",
			sparkline: {
				enabled: true
			}
		};
		this.series = [percent];
		this.colors = ["#A9FF01"];
		this.plotOptions = {
			radialBar: {
				offsetY: -20,
				startAngle: 0,
				hollow: {
					size: "45%",
				},
				track: {
					show: true,
				},
				dataLabels: {
					name: {
						show: false,
					},
					value: {
						fontSize: "2rem",
						show: true,
						offsetY: 10,
						formatter: function () {
							return `${reporters}/${totalReporters}` ;
						}
					},
				},
				
			},
		};
		this.stroke = {
			dashArray: 0
		};
	}
}
