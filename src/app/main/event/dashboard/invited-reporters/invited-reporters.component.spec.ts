import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitedReportersComponent } from './invited-reporters.component';

describe('InvitedReportersComponent', () => {
  let component: InvitedReportersComponent;
  let fixture: ComponentFixture<InvitedReportersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvitedReportersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitedReportersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
