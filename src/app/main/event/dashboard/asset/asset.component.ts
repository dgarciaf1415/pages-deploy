import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";
import { AssetModalService } from "app/layout/components/asset-modal/asset-modal.service";

@Component({
	selector: "app-asset",
	templateUrl: "./asset.component.html",
	styleUrls: ["./asset.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class AssetComponent implements OnInit {
	@Input() commentsQuantity: number;
	@Input() description: string;
	@Input() objective: string;
	@Input() objectiveApplied: boolean;
	@Input() reporterComments: any[];
	@Input() samplesQuantity: number;
	@Input() samplesQuantityAverage: number;
	@Input() type: string;
	@Input() url: string;
	@Input() wasView: boolean;

	samplesPercentage: number;

	constructor(private _assetModalService: AssetModalService) {}

	ngOnInit(): void {
		this.samplesPercentage = (this.samplesQuantityAverage * 100) / this.samplesQuantity;
	}

	openAssetModal() {
		const assets = {
			asset: this.url,
			assetType: this.type,
			assetDescription: this.description,
			assetSamples: this.samplesQuantity,
			assetSamplesAverage: this.samplesQuantityAverage,
			commentsQuantity: this.commentsQuantity,
			display: true,
			reporters: this.reporterComments,
			wasView: this.wasView,
		};
    this._assetModalService.open(assets);
  }

	ngOnDestroy(): void {
		this._assetModalService.close();
  }
}
