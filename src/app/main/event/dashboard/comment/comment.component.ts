import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";

import { CommentsModalService } from "app/layout/components/comments-modal/comments-modal.service";

@Component({
	selector: "app-comment",
	templateUrl: "./comment.component.html",
	styleUrls: ["./comment.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class CommentComponent implements OnInit {
	@Input() reporter: string;
	@Input() date: Date;
	@Input() images: any[];
	@Input() surveyType: string;

	public maxImagesPerRow: number;
	public maxCommentsPerRow: number;
	public indexCommentInArray: number;

	constructor(private _commentsModalService: CommentsModalService) {
		this.maxImagesPerRow = 4;
		this.maxCommentsPerRow = 1;
	}

	ngOnInit(): void {
		this.indexCommentInArray = this.images.findIndex((item) => {
			return !!item.comment;
		});
	}

	openCommentModal() {
		const reporterImages = {
			reporterName: this.reporter,
			date: this.date,
			display: true,
			images: this.images,
		};
		
    this._commentsModalService.open(reporterImages);
  }

	ngOnDestroy(): void {
		this._commentsModalService.close();
  }
}
