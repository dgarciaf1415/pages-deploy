import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { DashboardService } from "./dashboard.service";
import { environment } from "environments/environment";
import { catchError } from "rxjs/operators";
import { of } from "rxjs";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
  @ViewChild("pdfReport", { static: false }) pdfReport: ElementRef;

  public colors: string[];
  public enableSavingTemporary: boolean;
  public event: any;
  public eventReportUrl: string;
  public eventId: string;
  public filter: string = "ALL";
  public labels: string[] = [];
  public imageContainer: string;
  public isPublic: boolean;
  public objectivesList = [];
  public objectiveSelected: string;
  public progressBarValue: number;
  public reportersList: any[];
  public reporterComments: any[];
  public reporterSelected: number;
  public series: number[] = [];
  public total: string;
  public tooltipTexts: any;

  constructor(
    private route: ActivatedRoute,
    private _dashboardService: DashboardService,
  ) {
    this.enableSavingTemporary = environment.enableLocalStorage;
    this.objectiveSelected = '';
    this.imageContainer = environment.spaceUrl;
    this.isPublic = false;
    this.progressBarValue = 0;
    this.reportersList = [];
    this.reporterSelected = null;

    if (this.route.snapshot.paramMap.get("id")) {
      this.eventId = this.route.snapshot.paramMap.get("id");
      this.loadEvent(this.eventId);
    }

    this.tooltipTexts = {
      overviewReporters: 'Reporters are the stakeholders who have been assigned to validate the event\'s KPIs',
      objetives: ''
    }
  }

  loadEvent(id: any) {
    const responseLocal = this.enableSavingTemporary ? localStorage.getItem(`dashboard_data_${this.eventId}`) : null;
    if (responseLocal) {
      return this.loadContent(JSON.parse(responseLocal));
    }

    const setTimerBar = setInterval(() => {
      this.progressBarValue++;
    }, 900);

    if (isNaN(id)) {
      this.isPublic = true;
      this._dashboardService.getByUrl(id)
        .pipe(
          catchError((err) => {
            return of(err);
          })
        )
        .subscribe(async (res) => {
          if (res.error) {
            const progressBar = document.getElementById('ox--progress-bar');
            progressBar.classList.add('error-loading')
          } else {
            this.progressBarValue = 100;
            await this.timeout(500);
            this.loadContent(res);
          }

          clearInterval(setTimerBar);
        });
    } else {
      this.isPublic = false;
      this._dashboardService.getById(id.toString())
        .pipe(
          catchError((err) => {
            return of(err);
          })
        )
        .subscribe(async (res) => {
          if (res.error) {
            const progressBar = document.getElementById('ox--progress-bar');
            progressBar.classList.add('error-loading')
          } else {
            this.progressBarValue = 100;
            await this.timeout(500);
            this.loadContent(res);
          }

          clearInterval(setTimerBar);
        });
    }
  }

  async loadContent(response: any) {
    const assetsExisting = [];
    const assetAnswerReporters = {};
    const targets = {};
    let countTargets = 0;
    let sumAttendanceTarget1 = 0;
    let sumAttendanceTarget2 = 0;
    let totalAttendance = 0;

    if (this.enableSavingTemporary && !localStorage.getItem(`dashboard_data_${this.eventId}`)) {
      localStorage.setItem(`dashboard_data_${this.eventId}`, JSON.stringify(response));
    }

    this.eventReportUrl = response.reportUrl;    

    this.event = response;
    this.colors = ["#A9FF01", "#5CFEE4", "#424B5A"];

    this.event.reporterAnswers.forEach((cat) => {
      if (cat.category === 'Target audience') {
        cat.questions.forEach((answer) => {
          answer.answers.forEach((ans) => {
            if (!targets[ans.questionLabel.toLowerCase().replace(/ /g,'_')]) {
              targets[ans.questionLabel.toLowerCase().replace(/ /g,'_')]
              targets[ans.questionLabel.toLowerCase().replace(/ /g,'_')] = []
            }

            targets[ans.questionLabel.toLowerCase().replace(/ /g,'_')].push(Number(ans.answer))
          });
        });
      } else if (cat.category === 'Assets verification') {
        cat.questions.forEach((answer) => {
          answer.answers.forEach((ans) => {
            const assetInfo = this.event.brandAssets.find((asset) => {
              return asset.url === ans.answer;
            })

            if (assetInfo) {
              assetsExisting.push(assetInfo.url); 
            }
            
          });
        });
      } else if (cat.category === 'Assets verification for key stakeholders') {
        cat.questions.forEach((answer) => {
          answer.answers.forEach((ans) => {
            const imageNumber = ans.questionLabel.match(/\d+/)[0];
            
            if (!assetAnswerReporters[`reporter${ans.reporterId}_${imageNumber}`]) {
              assetAnswerReporters[`reporter${ans.reporterId}_${imageNumber}`] = {
                id: ans.reporterId,
                reporter: ans.reporter,
                image: ans.questionLabel,
                value: ans.answer.toLowerCase(),
                samplesQuantity: null,
                comment: '',
                date: '',
              }
            } else {
              assetAnswerReporters[`reporter${ans.reporterId}_${imageNumber}`] = {
                ...assetAnswerReporters[`reporter${ans.reporterId}_${imageNumber}`],
                samplesQuantity: Number(ans.answer),
              }
            }
          });
        });
      }
    });

    this.event.eventAssetComments.forEach((eventAsset) => {
      const imageNumber = eventAsset.image.match(/\d+/)[0];

      assetAnswerReporters[`reporter${eventAsset.reporterId}_${imageNumber}`] = {
        ...assetAnswerReporters[`reporter${eventAsset.reporterId}_${imageNumber}`],
        comment: eventAsset.comment,
        date: eventAsset.date,
      }
    });

    this.reporterComments = Object.values(this.event.eventFeedbackComments).map((item: any) => {
      this.reportersList.push({
        id: item.reporterId,
        fullname: item.reporter,
      });
      return {
        reporterId: item.reporterId,
        reporterName: item.reporter,
        date: item.date, 
        surveyType: item.surveyType,
        images: item.imageAndComments.map((subitem: any) => {
          return {
            id: subitem.image ? `image-${subitem.image.match(/\d+/)[0]}` : '',
            comment: subitem.comment,
            image: subitem.image ? `${this.imageContainer}/reporter-attachments/${subitem.image}` : '',
          };
        }),
      }
    });

    this.event.brandAssets = this.event.brandAssets.map((asset) => {
      this.objectivesList.push(asset.objective);
      const commentsQuantity = Object.values(assetAnswerReporters).filter((ans: any) => {
        return ans.image === asset.url && ans.comment !== '';
      });
      const reporterAnswer: any[] = Object.values(assetAnswerReporters).filter((ans: any) => {
        return ans.image === asset.url;
      });
      const samplesQuantityTotalReported = reporterAnswer.reduce((accum: number, current: any) =>  accum + current.samplesQuantity, 0 );

      return {
        ...asset,
        wasView: !!assetsExisting.includes(asset.url),
        objectiveApplied: !!reporterAnswer.find((ans: any) => {
          return ans.value === 'good';
        }),
        samplesQuantityTotalReported,
        samplesQuantityAverage: samplesQuantityTotalReported !== 0 ? (Number(samplesQuantityTotalReported) / reporterAnswer.length).toFixed(1).replace('.0','') : 0,
        comments: reporterAnswer.map((ans: any) => {
          return {
            name: ans.reporter,
            date: ans.date,
            comment: ans.comment,
          };
        }).filter((ans) => !!ans.comment),
        commentsQuantity: commentsQuantity.length,
      };
    });

    this.objectivesList = this.objectivesList.filter((item, index) => this.objectivesList.indexOf(item) === index).sort();
    
    for (const key in targets) {
      if (Object.prototype.hasOwnProperty.call(targets, key)) {
        countTargets = Object.keys(targets).indexOf(key);
        const element = targets[key];
        
        for (const attendanceReported of element) {
          if (countTargets === 0) {
            sumAttendanceTarget1 += attendanceReported;
          } else {
            sumAttendanceTarget2 += attendanceReported;
          }
        }
      }
    }

    this.series =
      this.event.reportersAttended > 0
        ? [
          countTargets > 0 ? parseFloat((sumAttendanceTarget2 / this.event.reportersAttended).toFixed(2)) : null,
             parseFloat((sumAttendanceTarget1 / this.event.reportersAttended).toFixed(2)),
          ].filter((elem) => elem !== null)
        : [0];
    
    this.event.attendanceExpectedPerTarget.forEach((element: any) => {
      totalAttendance += parseInt(element.attendance);

      this.labels.push(element.name);
    });

    this.total = `${totalAttendance}%`;
  }

  onSelected(value: string): void {
    this.filter = value;
  }

  attachments() {
    return this.event.reporterAttachments.attachments.filter((item) => {
      return this.filter === "ALL" || item.reporter === this.filter;
    });
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  ngOnInit(): void {}
}
