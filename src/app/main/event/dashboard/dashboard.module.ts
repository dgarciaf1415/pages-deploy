import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgApexchartsModule } from "ng-apexcharts";
import { CoreCardModule } from "@core/components/core-card/core-card.module";
import { AutocompleteLibModule } from "angular-ng-autocomplete";

import { DashboardComponent } from "./dashboard.component";
import { AssetComponent } from "./asset/asset.component";
import { CommentComponent } from "./comment/comment.component";
import { HeaderComponent } from "./header/header.component";
import { DashboardDescriptionComponent } from "./dashboard-description/dashboard-description.component";
import { TotalReportersComponent } from "./total-reporters/total-reporters.component";
import { InvitedReportersComponent } from "./invited-reporters/invited-reporters.component";
import { AttachmentComponent } from "./attachment/attachment.component";
import { DashboardService } from "./dashboard.service";
import { InviteComponent } from './invite/invite.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CoreCommonModule } from "@core/common.module";
import { ModalModule } from "app/layout/components/modal/modal.module";
import { FrequencyComponent } from './frequency/frequency.component';
import { CpmiRoeiComponent } from './cpmi-roei/cpmi-roei.component';
import { UtilsModule } from "app/utils/utils.module";
import { AssetModalModule } from "app/layout/components/asset-modal/asset-modal.module";
import { CommentsModalModule } from "app/layout/components/comments-modal/comments-modal.module";
import { AttendanceComponent } from './attendance/attendance.component';

const routes: Routes = [];

@NgModule({
  declarations: [
    DashboardComponent,
    AssetComponent,
    CommentComponent,
    HeaderComponent,
    DashboardDescriptionComponent,
    TotalReportersComponent,
    InvitedReportersComponent,
    AttachmentComponent,
    InviteComponent,
    FrequencyComponent,
    CpmiRoeiComponent,
    AttendanceComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreCardModule,
    NgbModule,
    CoreCommonModule,
    NgApexchartsModule,
    ModalModule,
    AssetModalModule,
    CommentsModalModule,
    AutocompleteLibModule,
    UtilsModule,
  ],
  providers: [DashboardService],
})
export class DashboardModule {}
