import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ApexAxisChartSeries, ApexChart, ApexXAxis, ApexDataLabels, ApexGrid, ApexStroke, ApexLegend, ApexTitleSubtitle, ApexTooltip, ApexPlotOptions, ApexYAxis, ApexFill, ApexMarkers, ApexTheme } from 'ng-apexcharts';

// interface ChartOptions
export interface ChartOptions {
  series?: ApexAxisChartSeries;
  chart?: ApexChart;
  xaxis?: ApexXAxis;
  dataLabels?: ApexDataLabels;
  grid?: ApexGrid;
  stroke?: ApexStroke;
  legend?: ApexLegend;
  title?: ApexTitleSubtitle;
  colors?: string[];
  tooltip?: ApexTooltip;
  plotOptions?: ApexPlotOptions;
  yaxis?: ApexYAxis;
  fill?: ApexFill;
  labels?: string[];
  markers: ApexMarkers;
  theme: ApexTheme;
}

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FrequencyComponent implements OnInit {
  @ViewChild('apexLineChartRef') apexLineChartRef: any;

  @Input() allEvents: string;
  @Input() eventsActivated: string;
  @Input() allAssets: string;
  @Input() assetsActivated: string;
  @Input() impressionsByEvent: any[];
  @Input() impressions: string;
  @Input() totalImpressions: string;
  @Input() projectedImpressions: string;
  @Input() samples: string;
  @Input() objetive: string;
  @Input() secondaryObjetive: string;
  @Input() objetiveTooltip: string;

  public apexLineChart: Partial<ChartOptions>;

  constructor() { }

  ngOnInit(): void {
    const eventImpressions = this.impressionsByEvent.map((imp) => imp.impressions.toFixed(1).replace(/\.00$/,''));
    const eventNames = this.impressionsByEvent.map((imp) => imp.eventName);

    // Apex Line Area Chart
    this.apexLineChart = {
      series: [
        {
          data: eventImpressions
        }
      ],
      chart: {
        height: 350,
        width: 480,
        type: 'line',
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        },
      },
      markers: {
        size: [4, 7],
        strokeWidth: 7,
        strokeOpacity: 1,
      },
      colors: ['#9600ff'],
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: 'straight',
        width: [3, 4],
        colors: ['#9600ff']
      },
      xaxis: {
        categories: eventNames,
        tickPlacement: 'between',
        labels: {
          show: true,
          trim: true
        }
      },
      tooltip: {
        custom: function (data) {
          return (
            '<div class="px-1 py-50">' +
            '<span>' +
            data.series[data.seriesIndex][data.dataPointIndex] +
            '</span>' +
            '</div>'
          );
        }
      }
    };
  }

}
