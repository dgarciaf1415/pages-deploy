import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import {
	ApexAxisChartSeries,
	ApexChart,
	ApexLegend,
	ApexNonAxisChartSeries,
	ApexPlotOptions,
	ApexStroke,
	ApexTooltip,
	ChartComponent,
} from "ng-apexcharts";

@Component({
	selector: "app-total-reporters",
	templateUrl: "./total-reporters.component.html",
	styleUrls: ["./total-reporters.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class TotalReportersComponent implements OnInit {
	@ViewChild("chart") chart: ChartComponent;

	@Input() eventAttendance: number;
	@Input() colors: string[];
	@Input() labels: string[];
	@Input() series: ApexAxisChartSeries | ApexNonAxisChartSeries;
	@Input() targets: any[];
	@Input() total: string;

	public chartOpt: ApexChart;
	public legend: ApexLegend;
	public plotOptions: ApexPlotOptions;
	public stroke: ApexStroke;
	public tooltip: ApexTooltip;

	constructor() {}

	ngOnInit(): void {
		this.tooltip = {
			enabled: true,
			y: {
				formatter: (percentage, { seriesIndex }) => {
					if(typeof percentage !== "undefined") {
						const attendanceExpected = Math.round((+this.eventAttendance * this.targets[seriesIndex].attendance) / 100);
						
						return percentage + '% [' + Math.round((+attendanceExpected * percentage) / 100) + '] reported';
					}
					return percentage;
				}
			}
		};
		this.chartOpt = {
			height: "340px",
			type: "radialBar",
		};
		this.plotOptions = {
			radialBar: {
				hollow: {
					size: "40%",
					margin: 5,
				},
				track: {
					background: "#C2D1D9",
					margin: 13,
				},
				dataLabels: {
					name: {
						fontSize: "22px",
					},
					total: {
						show: true,
						label: "Audience",
						formatter: () => {
							return this.total;
						},
					},
					value: {
						fontSize: "2rem",
					},
				},
			},
		};
		this.stroke = {
			lineCap: "round",
		};
		this.legend = {
			show: true,
			position: "bottom",
			horizontalAlign: "center",
			fontSize: "13px",
			itemMargin: {
				horizontal: 15,
				vertical: 0,
			},
			markers: {
				height: 16,
				width: 16,
			},
			formatter: (seriesName, opts) => {
				const currentTarget = this.targets[opts.seriesIndex];
				const quantityExpected = Math.round((this.eventAttendance * currentTarget.attendance) / 100);

        return seriesName + ': ' + currentTarget.attendance + '% [' + quantityExpected + '] expected';
    	}
		};
	}
}
