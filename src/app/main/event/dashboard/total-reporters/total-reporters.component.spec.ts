import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalReportersComponent } from './total-reporters.component';

describe('TotalReportersComponent', () => {
  let component: TotalReportersComponent;
  let fixture: ComponentFixture<TotalReportersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalReportersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalReportersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
