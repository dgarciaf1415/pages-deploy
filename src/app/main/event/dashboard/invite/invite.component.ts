import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { add, differenceInDays } from "date-fns";

import { ModalService } from "app/layout/components/modal/modal.service";
import { EventSinglesService } from "../../event-singles/event-singles.service";
import { ModalClose, ModalOptions } from "app/common/interfaces/modal.type";
import { ToastService } from "app/layout/components/toast/toast.service";
import { Reporter } from "app/common/interfaces/reporter.type";
import { ReportersService } from "app/main/admin/reporters/reporters.service";
import { environment } from "environments/environment";

@Component({
  selector: "app-invite",
  templateUrl: "./invite.component.html",
  styleUrls: ["./invite.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class InviteComponent implements OnInit {  
  public contentHeader: object;
  public completeReporters: object[];
  public displayModal: ModalOptions;
  public eventId: number;
  public pendingReporters: object[];
  public formReporter: FormGroup;
  public imageContainer: string;
  public isOutOfDate: boolean;
  public project: object;
  public eventName:String;
  public eventGroupName:String;
  public eventLocation:String;
  public reporters: object[];
  public reporterId: number;
  public eventReportUrl: string;
  public searchItem: string;
  public searchResults: string[];
  public searchSelected: Reporter;
  public webUrl: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private _eventsService: EventSinglesService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _reportersService: ReportersService
  ) {
    this.completeReporters = [];
    this.imageContainer = environment.spaceUrl;
    this.pendingReporters = [];
    this.project = {};
    this.reporters = [];
    this._unsubscribeAll = new Subject();
    this.webUrl = environment.adminUrl;

    if (this.route.snapshot.paramMap.get("id")) {
      this.eventId = parseInt(this.route.snapshot.paramMap.get("id"));

      if (this.eventId) {
        this.loadReportersByEvent(this.eventId);
      }
    }

    this.formReporter = this.fb.group({
      id: [],
      name: ["", Validators.required],
      organization: ["", Validators.required],
      phone: [""],
      email: ["", Validators.required],
      surveyType: ["", Validators.required],
      status: ["ACTIVE"],
    });
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Event reporters",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formReporter.valueChanges.subscribe(() => {
      if (this.formReporter.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.formReporter.get("email").valueChanges.subscribe((value) => {
      if (!!value && value.length > 7) {
        this._reportersService
          .search("email", value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((res) => {
            this.searchResults = res ? [res.email] : null;
            this.searchSelected = res;
          });
      }
    });
  }

  loadReportersByEvent(eventId: number) {
    this._eventsService
      .getReporters(eventId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.isOutOfDate = differenceInDays(new Date(), add(new Date(res.endDate), { hours: 5 })) > environment.maxDaysSinceEnd;
        this.eventReportUrl = res.reportUrl;

        this.pendingReporters = [];
        this.completeReporters = [];
        this.project = res.eventGroup.project;
        this.reporters = res.reporters;
        this.eventName=res.name;
        this.eventGroupName=res.eventGroup.name;
        this.eventLocation=res.eventGroup.location;

        for (const reporter of this.reporters as any) {
          if (reporter.status === "PENDING") {
            this.pendingReporters.push(reporter);
          } else {
            this.completeReporters.push(reporter);
          }
        }
      });
  }

  search() {
    this.pendingReporters = [];
    this.completeReporters = [];
    if (this.searchItem) {
      const reportersFiltered = this.reporters.filter((rep: any) => {
        return (
          rep.reporter.email.includes(this.searchItem) ||
          rep.reporter.name.includes(this.searchItem) ||
          rep.reporter.phone.includes(this.searchItem)
        );
      });

      for (const reporter of reportersFiltered as any) {
        if (reporter.status === "PENDING") {
          this.pendingReporters.push(reporter);
        } else {
          this.completeReporters.push(reporter);
        }
      }
    } else {
      for (const reporter of this.reporters as any) {
        if (reporter.status === "PENDING") {
          this.pendingReporters.push(reporter);
        } else {
          this.completeReporters.push(reporter);
        }
      }
    }
  }

  modalOpen(type: string, eventId: number, reporterId?: number) {
    this._modalService.open(type);

    if (eventId) {
      if (type === "create") {
        this._modalService.options(
          (this.displayModal = {
            title: "Assign reporter",
            buttonText: "Assign",
            enableButton: false,
          })
        );
      } else if (type === "delete") {
        this.reporterId = reporterId;
        const reporterToDelete: any = this.reporters.find((rep: any) => {
          return rep.reporter.id === this.reporterId;
        });

        if (reporterToDelete) {
          this._modalService.options(
            (this.displayModal = {
              title: "Unassign reporter",
              buttonText: "Yes, unassign",
              enableButton: false,
              itemType: "event reporter",
              itemName: reporterToDelete.reporter.email,
            })
          );
        }
      }
    }
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "create") {
        const alreadyExist = this.searchSelected
          ? this.searchSelected.email === this.formReporter.value.email
          : false;

        if (!alreadyExist) {
          delete this.formReporter.value.id;
          const surveyType = this.formReporter.value.surveyType;
          this._reportersService
            .create(this.formReporter.value)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res: any) => {
              this._eventsService
                .assignReporter(this.eventId, res.id, surveyType)
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                  this.loadReportersByEvent(this.eventId);
                  const savedText =
                    "Reporter was assigned to event successfully";
                  this._toastService.show(savedText, {
                    autohide: true,
                    headerTitle: "Success",
                    icon: "check",
                    iconColorClass: "text-success",
                  });
                  this._modalService.close();
                  this._modalService.options({
                    ...this.displayModal,
                    enableButton: false,
                  });
                });
            });
        } else {
          const reporterWasAssigned = this.reporters.find((rep: any) => {
            return rep.reporter.email === this.searchSelected.email;
          });

          if (!reporterWasAssigned) {
            this._eventsService
              .assignReporter(this.eventId, this.searchSelected.id, this.formReporter.get('surveyType').value)
              .pipe(takeUntil(this._unsubscribeAll))
              .subscribe(() => {
                this.loadReportersByEvent(this.eventId);
                const savedText = "Reporter was assigned to event successfully";
                this._toastService.show(savedText, {
                  autohide: true,
                  headerTitle: "Success",
                  icon: "check",
                  iconColorClass: "text-success",
                });
                this._modalService.close();
                this._modalService.options({
                  ...this.displayModal,
                  enableButton: false,
                });
              });
          } else {
            this._toastService.show("Reporter was assigned already", {
              autohide: true,
              headerTitle: "Success",
              icon: "check",
              iconColorClass: "text-success",
            });
          }
        }
      } else if (closeValues.type === "delete") {
        this._eventsService
          .unassignReporter(this.eventId, this.reporterId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.reporterId = null;
            this.loadReportersByEvent(this.eventId);
            const savedText = "Reporter was unassigned to event successfully";
            this._toastService.show(savedText, {
              autohide: true,
              headerTitle: "Success",
              icon: "check",
              iconColorClass: "text-success",
            });
            this._modalService.close();
            this._modalService.options({
              ...this.displayModal,
              enableButton: false,
            });
          });
      }
    }

    this.reporterId = null;
    this.searchResults = [];
    this.formReporter.reset();
    this.formReporter.get("status").setValue("ACTIVE");

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });
  }

  clearResults() {
    this.searchResults = [];
    this.formReporter.reset();
    this.formReporter.get("status").setValue("ACTIVE");
  }

  selectResult() {
    const elem = this.searchSelected;

    this.formReporter.get("id").setValue(elem.id);
    this.formReporter.get("name").setValue(elem.name);
    this.formReporter.get("organization").setValue(elem.organization);
    this.formReporter.get("phone").setValue(elem.phone);
    this.formReporter.get("email").setValue(elem.email, { emitEvent: false });
    this.formReporter.get("status").setValue("ACTIVE");
  }

  resendEmail(reporterId: number) {
    const elem = document.getElementById(`btn_resend_reporter_${reporterId}`);
    elem.setAttribute("disabled", "disabled");

    if (reporterId && this.eventId) {
      this._eventsService
        .resendEmailToReporter(reporterId, this.eventId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
          this.reporterId = null;
          this.loadReportersByEvent(this.eventId);
          elem.removeAttribute("disabled");
          const savedText = "Invitation was sent successfully";
          this._toastService.show(savedText, {
            autohide: true,
            headerTitle: "Success",
            icon: "check",
            iconColorClass: "text-success",
          });
          this._modalService.close();
          this._modalService.options({
            ...this.displayModal,
            enableButton: false,
          });
        });
    }
  }

  copyToClipboard() {
		const reportUrl = `${this.webUrl}/survey/login/${this.eventReportUrl}`;
		const selectBox = document.createElement('textarea');
		
    selectBox.style.position = 'fixed';
    selectBox.value = reportUrl;
    document.body.appendChild(selectBox);
    selectBox.focus();
    selectBox.select();
    document.execCommand('copy');
    document.body.removeChild(selectBox);

		/* Alert the copied text */
		this._toastService.show('URL copied', {
			autohide: true,
			headerTitle: "Success",
			icon: "check",
			iconColorClass: "text-success",
		});
	}

  updateSurveyType(event: any, eventReporterId: number) {
    this._eventsService.updateSurveyType(eventReporterId, event.target.value)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.loadReportersByEvent(this.eventId);
      });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
