import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class EventDataService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/event-data`;
  }

  getEventTypes() {
    const url = `${this.apiUrl}/event-types`;
    return this.http.get(url);
  }

  getEventVenues() {
    const url = `${this.apiUrl}/event-venues`;
    return this.http.get(url);
  }

  getEventGenders() {
    const url = `${this.apiUrl}/genders`;
    return this.http.get(url);
  }

  getEventEthnicities() {
    const url = `${this.apiUrl}/ethnicities`;
    return this.http.get(url);
  }

  getEventObjetives() {
    const url = `${this.apiUrl}/event-objetives`;
    return this.http.get(url);
  }

  getAssetTypes() {
    const url = `${this.apiUrl}/asset-types`;
    return this.http.get(url);
  }

  getEventObjetivesByEventType(eventType: number) {
    const url = `${this.apiUrl}/event-objetives/${eventType}`;
    return this.http.get(url);
  }

  getEventAssetsByEventType(eventType: number) {
    const url = `${this.apiUrl}/asset-types/${eventType}`;
    return this.http.get(url);
  }

  getAssetObjetives() {
    const url = `${this.apiUrl}/asset-objetives`;
    return this.http.get(url);
  }
}
