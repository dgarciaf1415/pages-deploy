import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Observable, Subject } from "rxjs";
import { format } from "date-fns";
import Stepper from "bs-stepper";
import { NgbDateStruct, NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";

import { environment } from "environments/environment";
import { ModalClose, ModalOptions } from "app/common/interfaces/modal.type";
import { File } from "app/common/interfaces/file.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";
import { Project } from "app/common/interfaces/project.type";
import { ProjectsService } from "../projects/projects.service";
import { EventGroup } from "app/common/interfaces/event-group.type";
import { EventGroupsService } from "./event-groups.service";
import { takeUntil } from "rxjs/operators";
import { EventDataService } from "./event-data.service";
import { EventSinglesService } from "../event-singles/event-singles.service";
import { Event } from "app/common/interfaces/event-single.type";
import { ReportersService } from "app/main/admin/reporters/reporters.service";
import { Reporter } from "app/common/interfaces/reporter.type";
import { ToastService } from "app/layout/components/toast/toast.service";
import { ModalService } from "app/layout/components/modal/modal.service";
import { Validations } from "app/utils/validations/validations";

@Component({
  selector: "app-event-groups",
  templateUrl: "./event-groups.component.html",
  styleUrls: ["./event-groups.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class EventGroupsComponent implements OnInit {
  public ageRange: object[];
  public assetQuantity: number;
  public assetQuantityArray: number[];
  public configSliderAge: object;
  public contentHeader: object;
  public dataForEvent: boolean;
  public displayModal: ModalOptions;
  public eventGroupCalendarStartDate: NgbDateStruct;
  public eventGroupCalendarEndDate: NgbDateStruct;
  public eventCalendarStartDate: NgbDateStruct;
  public eventCalendarEndDate: NgbDateStruct;
  public eventGroups: EventGroup[];
  public formEventGroup: FormGroup;
  public formEvent: FormGroup;
  public formReporter: FormGroup;
  public formType: string;
  public formStructType: string;
  public goToNext: boolean;
  public imageContainer: string;
  public maxAssets: number;
  public maxTargets: number;
  public pagination: PaginationOptions;
  public projectId: number;
  public reporters: object[];
  public searchItem: string;
  public targetQuantity: number;
  public targetQuantityArray: number[];
  public searchResults: string[];
  public searchSelected: Reporter;

  public $project: Observable<Project>;
  public $eventTypes: Observable<any>;
  public $assetObjetives: Observable<any>;
  public $eventVenues: Observable<any>;
  public $eventObjetives: Observable<any>;
  public $eventGenders: Observable<any>;
  public $eventEthnicities: Observable<any>;
  public $eventAssetTypes: Observable<any>;

  // Private
  private _unsubscribeAll: Subject<any>;
  private eventGroupWizard: Stepper;
  private eventWizard: Stepper;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private _projectsService: ProjectsService,
    private _eventGroupsService: EventGroupsService,
    private _eventsService: EventSinglesService,
    private _eventDataService: EventDataService,
    private _reportersService: ReportersService,
    private _toastService: ToastService,
    private _modalService: ModalService
  ) {
    this.assetQuantity = 0;
    this.assetQuantityArray = [];
    this.imageContainer = environment.spaceUrl;
    this.ageRange = [];
    this.configSliderAge = {
      behaviour: "drag",
      connect: true,
      margin: 3,
      limit: 70,
      tooltips: true,
      step: 1,
      range: {
        min: 13,
        max: 65,
      },
      pips: {
        mode: "range",
        density: 5,
      },
    };
    this.maxAssets = 40;
    this.maxTargets = 2;
    this.targetQuantity = 0;
    this.targetQuantityArray = [];
    this._unsubscribeAll = new Subject();
    this.eventGroups = [];
    this.reporters = [];

    this.pagination = {
      limit: 6,
      offset: 0,
      total: 0,
    };

    if (this.route.snapshot.paramMap.get("id")) {
      this.projectId = parseInt(this.route.snapshot.paramMap.get("id"));
      this.loadProject(this.projectId);
    }

    this.formEventGroup = this.fb.group({
      id: [],
      name: ["", Validators.required],
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      eventType: ["", Validators.required],
      eventVenue: ["", Validators.required],
      platformUrl: [""],
      location: [""],
      status: ["ACTIVE"],
      project: [this.projectId],
    });

    this.formEvent = this.fb.group({
      id: [],
      name: ["", Validators.required],
      location: ["", Validators.required],
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      startTime: [""],
      endTime: [""],
      budget: ["", Validators.required],
      sponsorshipInvestment: ["", Validators.required],
      attendance: ["", Validators.required],
      eventObjetive: ["", Validators.required],
      eventSecondaryObjetive: [""],
      targets: this.fb.group({}),
      assets: this.fb.group({}),
      status: ["ACTIVE"],
      eventGroup: [""],
    }, {
      validator: Validations.compareTimes('startTime', 'endTime')
    });

    this.formReporter = this.fb.group({
      id: [],
      name: ["", Validators.required],
      organization: ["", Validators.required],
      phone: [""],
      email: ["", Validators.required],
      surveyType: ["", Validators.required],
      status: ["ACTIVE"],
    });

    this.formType = "group";
  }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Event Groups",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Home",
            isLink: true,
            link: "/",
          },
          {
            name: "Sample",
            isLink: false,
          },
        ],
      },
    };

    this.formEventGroup.valueChanges.subscribe(() => {
      if (!!this.formEventGroup.get("eventVenue").value) {
        if (this.formEventGroup.get("eventVenue").value.toString() === "1") {
          this.formEventGroup
            .get("location")
            .setValidators([Validators.required]);
          this.formEventGroup.get("platformUrl").clearValidators();
        } else if (
          this.formEventGroup.get("eventVenue").value.toString() === "2"
        ) {
          this.formEventGroup.get("location").clearValidators();
          this.formEventGroup
            .get("platformUrl")
            .setValidators([Validators.required]);
        } else if (
          this.formEventGroup.get("eventVenue").value.toString() === "3"
        ) {
          this.formEventGroup
            .get("location")
            .setValidators([Validators.required]);
          this.formEventGroup
            .get("platformUrl")
            .setValidators([Validators.required]);
        }
        this.formEventGroup
          .get("location")
          .updateValueAndValidity({ emitEvent: false });
        this.formEventGroup
          .get("platformUrl")
          .updateValueAndValidity({ emitEvent: false });
      }

      if (this.formEventGroup.valid) {
        this._modalService.options({
          ...this.displayModal,
          enableButton: true,
        });
      } else {
        this._modalService.options({
          ...this.displayModal,
          enableButton: false,
        });
      }
    });

    this.formReporter.get("email").valueChanges.subscribe((value) => {
      if (!!value && value.length > 7) {
        this._reportersService
          .search("email", value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((res) => {
            this.searchResults = res ? [res.email] : null;
            this.searchSelected = res;
          });
      }
    });

    this.route.params.subscribe((params) => {
      this.projectId = params["id"];
      this.loadProject(this.projectId);
    });
  }

  loadProject(projectId: number) {
    this.$project = this._projectsService.getById(projectId);
    this.loadEventGroups(this.projectId, this.pagination);
  }

  loadEventGroups(projectId: number, pagination: PaginationOptions) {
    this._eventGroupsService
      .getAllByProject(projectId, pagination)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((group: any) => {
        this.eventGroups = group.rows;

        this.pagination = {
          limit: pagination.limit,
          offset: pagination.offset,
          total: group.count,
        };
      });
  }

  loadDataforEvent() {
    const eventType = this.eventGroups.find((group) => {
      return group.id === parseInt(this.formEvent.value.eventGroup);
    });

    this.$eventGenders = this._eventDataService.getEventGenders();
    this.$eventEthnicities = this._eventDataService.getEventEthnicities();
    this.$assetObjetives = this._eventDataService.getAssetObjetives();

    if (eventType) {
      this.dataForEvent = true;
      const startDate = format(
        new Date(eventType.startDate.slice(0, -1)),
        "yyyy-MM-dd"
      );
      const endDate = format(
        new Date(eventType.endDate.slice(0, -1)),
        "yyyy-MM-dd"
      );

      this.formEventGroup.get("startDate").setValue({
        year: parseInt(startDate.split("-")[0]),
        month: parseInt(startDate.split("-")[1]),
        day: parseInt(startDate.split("-")[2]),
      });
      this.formEventGroup.get("endDate").setValue({
        year: parseInt(endDate.split("-")[0]),
        month: parseInt(endDate.split("-")[1]),
        day: parseInt(endDate.split("-")[2]),
      });

      this.$eventObjetives =
        this._eventDataService.getEventObjetivesByEventType(
          (eventType.eventType as any).id
        );
      this.$eventAssetTypes = this._eventDataService.getAssetTypes();

    } else {
      this.dataForEvent = false;
      this.$eventObjetives =
        this._eventDataService.getEventObjetivesByEventType(0);
      this.$eventAssetTypes =
        this._eventDataService.getAssetTypes();
    }
  }

  modalOpen(type: string, valueId: number, form: string) {
    this.formStructType = type;
    this._modalService.open(type);
    this.formType = form;
    if (form === 'group') {
      if (valueId) {
        if (type === "edit") {
          this._eventGroupsService
            .getById(valueId)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((group) => {
              const startDate = format(
                new Date(group.startDate.slice(0, -1)),
                "yyyy-MM-dd"
              );
              const endDate = format(
                new Date(group.endDate.slice(0, -1)),
                "yyyy-MM-dd"
              );

              this.formEventGroup.get("id").setValue(group.id);
              this.formEventGroup.get("name").setValue(group.name);
              this.formEventGroup.get("startDate").setValue({
                year: parseInt(startDate.split("-")[0]),
                month: parseInt(startDate.split("-")[1]),
                day: parseInt(startDate.split("-")[2]),
              });
              this.formEventGroup.get("endDate").setValue({
                year: parseInt(endDate.split("-")[0]),
                month: parseInt(endDate.split("-")[1]),
                day: parseInt(endDate.split("-")[2]),
              });
              this.formEventGroup
                .get("eventType")
                .setValue((group.eventType as any).id);
              this.formEventGroup
                .get("eventVenue")
                .setValue((group.eventVenue as any).id);
              this.formEventGroup.get("location").setValue(group.location);
              this.formEventGroup.get("platformUrl").setValue(group.platformUrl);

              this.formEventGroup.get("eventType").disable();
            });
          this._modalService.options(
            (this.displayModal = {
              title: "Edit event group",
              buttonText: "Update",
              enableButton: false,
            })
          );
        } else if (type === "duplicate") {
          this.formEventGroup.get("id").setValue(valueId);
          this._modalService.open('duplicate');
          this._modalService.options(
            (this.displayModal = {
              title: "Are you sure?",
              buttonText: "Duplicate",
              enableButton: false,
              description: "Duplicate <b>Event Group</b>",
            })
          );
        } else {
          this.formEventGroup.get("id").setValue(valueId);
          this.formEventGroup.get("eventType").enable();
          this._modalService.options(
            (this.displayModal = {
              title: "Delete event group",
              buttonText: "Yes, delete",
              enableButton: false,
              itemType: "group",
              itemName: this.eventGroups.find((group) => {
                return group.id === valueId;
              }).name,
            })
          );
        }
      } else {
        this.formEventGroup.get("eventType").enable();
        this._modalService.options(
          (this.displayModal = {
            title: "",
            buttonText: "Create",
            enableButton: false,
          })
        );
      }
    } else if (form === 'event') {
      if (type === "duplicate") {
        this.formEvent.get("id").setValue(valueId);
        this._modalService.open('duplicate');
        this._modalService.options(
          (this.displayModal = {
            title: "Are you sure?",
            buttonText: "Duplicate",
            enableButton: false,
            description: "Duplicate <b>Event</b>",
          })
        );
      } else if (type === "delete") {
        this.formEvent.get("id").setValue(valueId);
        const eventGroup: any = this.eventGroups.find((group: any) => {
          return group.events.find((event: any) => {
            return event.id === valueId;
          });
        })
        const eventName = !!eventGroup ? eventGroup.events[0].name : null

        this._modalService.options(
          (this.displayModal = {
            title: "Delete event",
            buttonText: "Yes, delete",
            enableButton: false,
            itemType: "event",
            itemName: eventName,
          })
        );
      }
    }

    this.$eventTypes = this._eventDataService.getEventTypes();
    this.$eventVenues = this._eventDataService.getEventVenues();

    this.addNewTarget();
    this.addNewAsset();
  }

  changeModalStatus(closeValues: ModalClose) {
    if (closeValues.reason === "accept") {
      if (closeValues.type === "edit") {
        this._eventGroupsService
          .update(
            this.formEventGroup.get("id").value,
            this.getDirtyValues(this.formEventGroup)
          )
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            this.loadEventGroups(this.projectId, this.pagination);
            const savedText = "Event Group was updated successfully";
            this._toastService.show(savedText, {
              autohide: true,
              headerTitle: "Success",
              icon: "check",
              iconColorClass: "text-success",
            });
          });
      } else if (closeValues.type === "delete") {
        if (closeValues.form === 'event') {
          this._eventsService
            .delete(this.formEvent.get("id").value)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.loadEventGroups(this.projectId, this.pagination);
              const savedText = "Event was deleted successfully";
              this._toastService.show(savedText, {
                autohide: true,
                headerTitle: "Success",
                icon: "check",
                iconColorClass: "text-success",
              });
            });
        } else {
          this._eventGroupsService
            .delete(this.formEventGroup.get("id").value)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
              this.loadEventGroups(this.projectId, this.pagination);
              const savedText = "Event Group was deleted successfully";
              this._toastService.show(savedText, {
                autohide: true,
                headerTitle: "Success",
                icon: "check",
                iconColorClass: "text-success",
              });
            });
        }
      } else if (closeValues.type === "duplicate") {
        if (this.formType === 'group') {
          this.duplicateGroup(this.formEventGroup.get("id").value);
        } else if (this.formType === 'event') {
          this.duplicateGroup(this.formEvent.get("id").value);
        }
      }
    }

    this._modalService.close();
    this._modalService.options({ ...this.displayModal, enableButton: false });

    this.cleanForms();
    this.goToNext = false;
  }

  checkModalStatus(isOpen: boolean) {
    if (isOpen) {
      setTimeout(() => {
        if (this.formType === "group") {
          this.eventGroupWizard = new Stepper(
            document.querySelector("#group-and-event-wizard"),
            {}
          );
        } else {
          this.eventWizard = new Stepper(
            document.querySelector("#event-wizard"),
            {}
          );
        }
      }, 80);
    }
  }

  addReporter() {
    if (this.formReporter.valid) {
      const alreadyExist = this.reporters.find((reporter: any) => {
        return reporter.email === this.formReporter.value.email;
      });

      if (!alreadyExist) {
        if (!this.formReporter.value.id) {
          delete this.formReporter.value.id;
          const surveyType = this.formReporter.value.surveyType;
          this._reportersService
            .create(this.formReporter.value)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
              this.reporters.push({
                ...res,
                surveyType,
              });
            });
        } else {
          this.reporters.push(this.formReporter.value);
        }
      }

      this.searchResults = [];
      this.formReporter.reset();
      this.formReporter.get("status").setValue("ACTIVE");
      this.formReporter.get("surveyType").setValue("");
    }
  }

  removeReporter(reporterEmail: string) {
    for (let i = 0; i < this.reporters.length; i++) {
      if ((this.reporters[i] as any).email === reporterEmail) {
        this.reporters.splice(i, 1);
        break;
      }
    }
  }

  addNewTarget() {
    if (this.targetQuantityArray.length < this.maxTargets) {
      const targets = this.formEvent.get("targets") as FormGroup;
      this.targetQuantity++;
      this.targetQuantityArray.push(this.targetQuantity);
      this.ageRange.push({
        id: this.targetQuantity,
        range: [5, 30],
      });

      targets.setControl(
        `target_${this.targetQuantity}`,
        this.fb.group({
          name: ["", Validators.required],
          expectedAttendance: ["", Validators.required],
          eventAgeMin: [5, Validators.required],
          eventAgeMax: [30, Validators.required],
          eventGender: ["", Validators.required],
          eventEthnicity: ["", Validators.required],
        })
      );
    }
  }

  addNewAsset() {
    if (this.assetQuantityArray.length < this.maxAssets) {
      const assets = this.formEvent.get("assets") as FormGroup;
      this.assetQuantity++;
      this.assetQuantityArray.push(this.assetQuantity);

      assets.setControl(
        `asset_${this.assetQuantity}`,
        this.fb.group({
          nameRaw: ['', Validators.required],
          name: ['', Validators.required],
          type: [null, Validators.required],
          objetive: ['', Validators.required],
          expectedQuantity: ['1', Validators.required],
          description: [''],
        })
      );
    }
  }

  deleteTarget(target: number) {
    const targets = this.formEvent.get("targets") as FormGroup;

    for (let i = 0; i < this.ageRange.length; i++) {
      if ((this.ageRange[i] as any).id === target) {
        this.ageRange.splice(i, 1);
        this.targetQuantityArray.splice(i, 1);
        break;
      }
    }

    targets.removeControl(`target_${target}`);
  }

  deleteAsset(asset: number) {
    const assets = this.formEvent.get("assets") as FormGroup;

    for (let i = 0; i < this.assetQuantityArray.length; i++) {
      if (this.assetQuantityArray[i] === asset) {
        this.assetQuantityArray.splice(i, 1);
        break;
      }
    }

    assets.removeControl(`asset_${asset}`);
  }

  stepperNext(data: FormGroupDirective, formType: string) {
    this.goToNext = true;
    if (!!data.form.valid) {
      this.goToNext = false;
      const button = document.getElementById(`btn-next-${formType}`);
      
      if (formType === "eventGroup") {
        if (this.formEventGroup.value.id === null) {
          const spinner = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>&nbsp; <span class="align-middle d-sm-inline-block">Publish</span>';
          const noSpinner = '<span class="align-middle d-sm-inline-block">Publish</span>';
          button.innerHTML = spinner;
          button.setAttribute('disabled', 'disabled');
          delete data.value.id;
          // clone
          const payload: EventGroup = JSON.parse(JSON.stringify(data.value));
          const eventType = this.formEventGroup.value.eventType;

          this.$eventGenders = this._eventDataService.getEventGenders();
          this.$eventEthnicities = this._eventDataService.getEventEthnicities();
          this.$eventObjetives =
            this._eventDataService.getEventObjetivesByEventType(eventType);
          this.$eventAssetTypes =
            this._eventDataService.getAssetTypes();
          this.$assetObjetives = this._eventDataService.getAssetObjetives();

          payload.startDate = this.dateFormat(data.value.startDate, null, "start");
          payload.endDate = this.dateFormat(data.value.endDate, null, "end");

          this._eventGroupsService
            .create(payload)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res: any) => {
              this.formEvent.get("eventGroup").setValue(res.id);
              button.innerHTML = noSpinner;
              button.removeAttribute('disabled');
              const savedText = "Event group was created successfully";
              this._toastService.show(savedText, {
                autohide: true,
                headerTitle: "Success",
                icon: "check",
                iconColorClass: "text-success",
              });
              this.loadEventGroups(this.projectId, this.pagination);
              if (this.formType === "group") {
                this.eventGroupWizard.next();
              } else {
                this.eventWizard.next();
              }
            });
        } else {
          if (this.formType === "group") {
            this.eventGroupWizard.next();
          } else {
            this.eventWizard.next();
          }
        }
      } else if (formType === "event") {
        if (this.formEvent.value.id === null) {
          const spinner = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>&nbsp; <span class="align-middle d-sm-inline-block">Next</span>';
          const noSpinner = '<span class="align-middle d-sm-inline-block">Next</span>';
          button.innerHTML = spinner;
          button.setAttribute('disabled', 'disabled');
          delete data.value.id;
          if (!data.value.eventSecondaryObjetive) {
            delete data.value.eventSecondaryObjetive;
          }

          // clone
          const payload: Event = JSON.parse(JSON.stringify(data.value));

          payload.startDate = this.dateFormat(data.value.startDate, data.value.startTime, "start");
          payload.endDate = this.dateFormat(data.value.endDate, data.value.endTime, "end");

          delete payload.startTime;
          delete payload.endTime;

          payload.targets = [];
          for (const key in data.value.targets) {
            if (Object.prototype.hasOwnProperty.call(data.value.targets, key)) {
              payload.targets.push(data.value.targets[key]);
            }
          }

          payload.assets = [];
          for (const key in data.value.assets) {
            if (Object.prototype.hasOwnProperty.call(data.value.assets, key)) {
              delete data.value.assets[key].nameRaw;
              payload.assets.push(data.value.assets[key]);
            }
          }

          this._eventsService
            .create(payload)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res: any) => {
              this.loadEventGroups(this.projectId, this.pagination);
              this.formEvent.get("id").setValue(res.id);
              button.innerHTML = noSpinner;
              button.removeAttribute('disabled');
              const savedText = "Event created was successfully";
              this._toastService.show(savedText, {
                autohide: true,
                headerTitle: "Success",
                icon: "check",
                iconColorClass: "text-success",
              });
              if (this.formType === "group") {
                this.eventGroupWizard.next();
              } else {
                this.eventWizard.next();
              }
            });
        } else {
          if (this.formType === "group") {
            this.eventGroupWizard.next();
          } else {
            this.eventWizard.next();
          }
        }
      }
    }
  }

  stepperPrevious() {
    if (this.formType === "group") {
      this.eventGroupWizard.previous();
    } else {
      this.eventWizard.previous();
    }
  }

  async uploadFile(file: any, fieldName: string) {
    const fileData = new FormData();
    const fileLoaded = file.target.files[0];
    const assets = this.formEvent.get("assets") as FormGroup;

    fileData.append("image", fileLoaded, fileLoaded.name);

    if (fileLoaded.size > 10000000) {
      this._toastService.show('The file uploaded exceed weight parameters', {
        autohide: true,
        headerTitle: "Error",
        icon: 'alert-circle',
        iconColorClass: "text-danger",
      });

      assets.get(`${fieldName}.nameRaw`).reset();
      assets.get(`${fieldName}.name`).reset();
      return;
    }

    if (fileLoaded) {
      await this._eventsService.upload(fileData).subscribe((f: File) => {
        assets.get(`${fieldName}.name`).setValue(f.filename);
      });
    }
  }

  setValues(fieldName: string, values: number[]) {
    const targets = this.formEvent.get("targets") as FormGroup;
    targets.get(`${fieldName}.eventAgeMin`).setValue(values[0]);
    targets.get(`${fieldName}.eventAgeMax`).setValue(values[1]);
  }

  duplicateGroup(groupId: number) {
    if (this.formType === 'group') {
      this._eventGroupsService.duplicate(groupId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
          this.loadEventGroups(this.projectId, this.pagination);
          this.formEventGroup.get("id").reset();
          const savedText = "Event Group was duplicated successfully";
          this._toastService.show(savedText, {
            autohide: true,
            headerTitle: "Success",
            icon: "check",
            iconColorClass: "text-success",
          });
        });
    } else if (this.formType === 'event') {
      const eventId = groupId;
      this._eventsService.duplicate(eventId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
          this.loadEventGroups(this.projectId, this.pagination);
          this.formEvent.get("id").reset();
          const savedText = "Event was duplicated successfully";
          this._toastService.show(savedText, {
            autohide: true,
            headerTitle: "Success",
            icon: "check",
            iconColorClass: "text-success",
          });
        });
    }
  }

  validatePercentage(elem: InputEvent, targetId: number) {
    const field: any = elem.target;
    const formField = this.formEvent.get(`targets.target_${targetId}.expectedAttendance`);
    let otherFormField;

    if (this.targetQuantityArray.length > 1) {
      if (this.targetQuantityArray[0] === targetId) {
        otherFormField = this.formEvent.get(`targets.target_${this.targetQuantityArray[1]}.expectedAttendance`);
      } else {
        otherFormField = this.formEvent.get(`targets.target_${this.targetQuantityArray[0]}.expectedAttendance`);
      }
    }

    if (field.value && parseInt(field.value) === 0) {
      field.value = 0;
      return formField.patchValue(0);
    } else if (field.value && parseInt(field.value) > 100) {
      field.value = 100;
      return formField.patchValue(100);
    }
    
    if (!!field.value && !!otherFormField?.value) {
      if ((parseInt(field.value) + parseInt(otherFormField.value)) > 100) {
        const newValue = 100 - parseInt(otherFormField.value);
        field.value = newValue;
        return formField.patchValue(newValue);
      }
    }
  }

  paginate(pagination: PaginationOptions) {
    this.loadEventGroups(this.projectId, pagination);
  }

  search() {
    this._eventGroupsService
      .search(this.projectId, this.pagination, this.searchItem)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.eventGroups = res.rows;

        this.pagination = {
          limit: this.pagination.limit,
          offset: this.pagination.offset,
          total: res.count,
        };
      });
  }

  onSubmit() {
    if (this.reporters.length > 0) {
      const spinner = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>&nbsp; Assign';
      const noSpinner = 'Assign';
      const button = document.getElementById('btn-submit');
      button.setAttribute('disabled', 'disabled')
      button.innerHTML = spinner;
      this.reporters.map((rep: any, i: number) => {
        this._eventsService
          .assignReporter(this.formEvent.value.id, rep.id, rep.surveyType)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(() => {
            if (i === this.reporters.length - 1) {
              button.innerHTML = noSpinner;
              button.removeAttribute('disabled');
              const savedText = "Reporters were assigned to event successfully";
              this._toastService.show(savedText, {
                autohide: true,
                headerTitle: "Success",
                icon: "check",
                iconColorClass: "text-success",
              });
              this._modalService.close();
              this._modalService.options({
                ...this.displayModal,
                enableButton: false,
              });
            }
          });
      });
    }
  }

  clearResults() {
    this.searchResults = [];
    this.formReporter.reset();
    this.formReporter.get("status").setValue("ACTIVE");
  }

  selectResult() {
    const elem = this.searchSelected;

    this.formReporter.get("id").setValue(elem.id);
    this.formReporter.get("name").setValue(elem.name);
    this.formReporter.get("organization").setValue(elem.organization);
    this.formReporter.get("phone").setValue(elem.phone);
    this.formReporter.get("email").setValue(elem.email, { emitEvent: false });
    this.formReporter.get("status").setValue("ACTIVE");
  }

  searchName(type: string, elem: any) {
    if (elem.target.value) {
      if (type === "group") {
        this._eventGroupsService
          .searchByType("name", elem.target.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((res) => {
            if (res) {
              this.formEventGroup.get("name").setErrors({ nameExist: true });
            } else {
              this.formEventGroup.get("name").setErrors(null);
            }
          });
      } else {
        this._eventsService
          .searchByType("name", elem.target.value)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((res) => {
            if (res) {
              this.formEvent.get("name").setErrors({ nameExist: true });
            } else {
              this.formEvent.get("name").setErrors(null);
            }
          });
      }
    }
  }

  dateFormat(value: any, timeRaw: any, type: string) {
    if (value) {
      const month =
        value.month.toString().length > 1 ? value.month : `0${value.month}`;
      const day = value.day.toString().length > 1 ? value.day : `0${value.day}`;
      let time = type === "start" ? "00:00:00" : "23:59:59";

      if (timeRaw) {
        time = this.timeFormat(timeRaw);
      }

      return `${value.year}-${month}-${day} ${time}`;
    }
  }

  timeFormat(value: any) {
    if (value) {
      const hour = value.hour.toString().length > 1 ? value.hour : `0${value.hour}`;
      const minute = value.minute.toString().length > 1 ? value.minute : `0${value.minute}`;

      return `${hour}:${minute}:00`;
    }
  }

  cleanForms() {
    this.formEventGroup.reset();
    this.formEventGroup.get("eventType").setValue("");
    this.formEventGroup.get("eventVenue").setValue("");
    this.formEventGroup.get("status").setValue("ACTIVE");
    this.formEventGroup.get("project").setValue(this.projectId);

    this.formEvent.reset();
    this.formEvent.get("eventGroup").setValue("");
    this.formEvent.get("eventObjetive").setValue("");
    this.formEvent.get("eventSecondaryObjetive").setValue("");
    this.formEvent.get("status").setValue("ACTIVE");

    for (let i = 0; i < this.targetQuantityArray.length; i++) {
      (this.formEvent.get("targets") as FormGroup).removeControl(
        `target_${this.targetQuantityArray[i]}`
      );
    }
    for (let i = 0; i < this.assetQuantityArray.length; i++) {
      (this.formEvent.get("assets") as FormGroup).removeControl(
        `asset_${this.assetQuantityArray[i]}`
      );
    }

    this.targetQuantity = 0;
    this.targetQuantityArray = [];
    this.assetQuantity = 0;
    this.assetQuantityArray = [];
    this.ageRange = [];
    this.dataForEvent = false;
    this.reporters = [];
    this.searchResults = [];
  }

  getDirtyValues(form: any) {
    let dirtyValues = {};
    let dateFields = ["startDate", "endDate"];

    Object.keys(form.controls).forEach((key) => {
      let currentControl = form.controls[key];

      if (currentControl.dirty) {
        if (currentControl.controls)
          dirtyValues[key] = this.getDirtyValues(currentControl);
        else
          dirtyValues[key] = dateFields.includes(key)
            ? key === dateFields[0]
              ? this.dateFormat(currentControl.value, null, "start")
              : this.dateFormat(currentControl.value, null, "end")
            : currentControl.value;
      }
    });

    return dirtyValues;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
