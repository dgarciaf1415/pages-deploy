import { NgModule } from "@angular/core";
import { CoreCommonModule } from "@core/common.module";
import { CoreDirectivesModule } from "@core/directives/directives";
import { NgbDateParserFormatter, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxMaskModule } from "ngx-mask";
import { NouisliderModule } from "ng2-nouislider";
import { AutocompleteLibModule } from "angular-ng-autocomplete";

import { ModalModule } from "app/layout/components/modal/modal.module";
import { EventGroupsComponent } from "./event-groups.component";
import { EventGroupsService } from "./event-groups.service";
import { EventDataService } from "./event-data.service";
import { PaginationModule } from "app/layout/components/pagination/pagination.module";
import { UtilsModule } from "app/utils/utils.module";
import { RouterModule } from "@angular/router";
import { NgbDateCustomParserFormatter } from "app/common/services/ngb-date-custom-parser-formatter";
import { NgSelectModule } from "@ng-select/ng-select";

@NgModule({
  declarations: [EventGroupsComponent],
  imports: [
    CoreCommonModule,
    NgbModule,
    NgxMaskModule.forRoot(),
    ModalModule,
    CoreDirectivesModule,
    NouisliderModule,
    PaginationModule,
    AutocompleteLibModule,
    UtilsModule,
    RouterModule,
    NgSelectModule,
  ],
  providers: [
    EventGroupsService,
    EventDataService,
    { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter },
  ],
})
export class EventGroupsModule {}
