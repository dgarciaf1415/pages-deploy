import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";

import { environment } from "environments/environment";
import {
  CreateEventGroupDto,
  EventGroup,
  UpdateEventGroupDto,
} from "app/common/interfaces/event-group.type";
import { PaginationOptions } from "app/common/interfaces/pagination.type";
import { ToastService } from "app/layout/components/toast/toast.service";

@Injectable({
  providedIn: "root",
})
export class EventGroupsService {
  apiUrl: string;

  constructor(private http: HttpClient, private _toastService: ToastService) {
    this.apiUrl = `${environment.apiUrl}/event-groups`;
  }

  getAllByProject(projectId: number, pagination: PaginationOptions) {
    // const url = `${this.apiUrl}/project/${projectId}?limit=${pagination.limit}&offset=${pagination.offset}`;
    const url = `${this.apiUrl}/project/${projectId}`;
    return this.http.get<EventGroup[]>(url);
  }

  search(projectId: number, pagination: PaginationOptions, search: string) {
    // const url = `${this.apiUrl}/project/search/${projectId}?limit=${pagination.limit}&offset=${pagination.offset}&search=${search}`;
    const url = `${this.apiUrl}/project/search/${projectId}?search=${search}`;
    return this.http.get<EventGroup[]>(url);
  }

  getById(groupId: number) {
    const url = `${this.apiUrl}/${groupId}`;
    return this.http.get<EventGroup>(url);
  }

  searchByType(type: string, search: string) {
    const url = `${this.apiUrl}/search/${type}/${search}`;
    return this.http.get<EventGroup>(url);
  }

  duplicate(groupId: number) {
    const url = `${this.apiUrl}/copy/${groupId}`;
    return this.http.get<EventGroup>(url);
  }

  create(body: CreateEventGroupDto) {
    const url = this.apiUrl;
    return this.http.post(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: "Error",
          icon: "alert-circle",
          iconColorClass: "text-danger",
        });

        return throwError(error);
      })
    );
  }

  update(groupId: number, body: UpdateEventGroupDto) {
    const url = `${this.apiUrl}/${groupId}`;
    return this.http.put(url, body).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: "Error",
          icon: "alert-circle",
          iconColorClass: "text-danger",
        });

        return throwError(error);
      })
    );
  }

  delete(groupId: number) {
    const url = `${this.apiUrl}/${groupId}`;
    return this.http.put(url, { status: "DELETED" }).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: "Error",
          icon: "alert-circle",
          iconColorClass: "text-danger",
        });

        return throwError(error);
      })
    );
  }
}
