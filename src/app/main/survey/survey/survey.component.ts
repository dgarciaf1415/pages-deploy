import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subject, of } from "rxjs";
import { catchError, takeUntil } from "rxjs/operators";

import { environment } from "environments/environment";
import { File } from "app/common/interfaces/file.type";
import { SurveyService } from "./survey.service";
import { AuthenticationService } from "app/auth/service";
import { ToastService } from "app/layout/components/toast/toast.service";
import { CoreConfigService } from "@core/services/config.service";

@Component({
  selector: "app-survey",
  templateUrl: "./survey.component.html",
  styleUrls: ["./survey.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class SurveyComponent implements OnInit {
  public assets: string[];
  public assetPositions: number[];
  public assetQuestions: any[];
  public assetsSelected: any[];
  public assetsIdsSelected: number[];
  public assetQuantity: number;
  public coreConfig: any;
  public configSliderGeneralOptions: object;
  public configSliderCapacityOptions: object;
  public configSliderTargetOptions: object;
  public currentAssetDisplayed: number;
  public currentFeedbackIndex: number;
  public currentStep: number;
  public eventData: any;
  public eventInfo: any;
  public feedbackImages: any[];
  public feedbackImageFiles: any[];
  public feedbackImageFilenames: string[];
  public formSurvey: FormGroup;
  public imageContainer: string;
  public isCurrentStepValid: boolean;
  public isSurveyExtended: boolean;
  public maxFeedbackImages: number[];
  public preloadImage: boolean;
  public projectData: any;
  public progressBarValue: number;
  public questionsForAssetsSelected: FormGroup[];
  public themeStyle: string;
  public steps: number;
  public isSavingSurvey: boolean;
  public survey: any;
  public validationsForAssetsSelected: string[];
  public isUploadingImage: boolean;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _surveyService: SurveyService,
    private fb: FormBuilder,
    private _authenticationService: AuthenticationService,
    private _router: Router,
    private _toastService: ToastService,
    private _coreConfigService: CoreConfigService
  ) {
    this._unsubscribeAll = new Subject();
    this.formSurvey = this.fb.group({});
    this.imageContainer = environment.spaceUrl;
    this.assets = [];
    this.assetPositions = [];
    this.assetsSelected = [];
    this.assetsIdsSelected = [];
    this.assetQuantity = 0;
    this.currentStep = 1;
    this.currentAssetDisplayed = 0;
    this.configSliderGeneralOptions = {
      behaviour: "tap",
      connect: "lower",
      tooltips: true,
      showTooltips: true,
      start: 0,
      step: 1,
      format: {
        postfix: "%",
        to: (value) => {
          return value + "%";
        },
        from: (value) => {
          return value.replace("%", "");
        },
      },
    };
    this.configSliderCapacityOptions = {
      ...this.configSliderGeneralOptions,
      margin: 3,
      range: {
        min: 0,
        // '17%': 20,
        // '34%': 40,
        // '51%': 60,
        // '68%': 80,
        // '85%': 100,
        max: 100,
      },
      pips: {
        mode: "count",
        density: 20,
        values: 6,
        format: {
          postfix: "%",
          to: (value) => {
            return value + "%";
          },
          from: (value) => {
            return value.replace("%", "");
          },
        },
      },
    };
    this.configSliderTargetOptions = {
      ...this.configSliderGeneralOptions,
      margin: 3,
      range: {
        min: 0,
        max: 100,
      },
      pips: {
        mode: "count",
        density: 20,
        values: 6,
        format: {
          postfix: "%",
          to: (value) => {
            return value + "%";
          },
          from: (value) => {
            return value.replace("%", "");
          },
        },
      },
    };
    this.feedbackImages = [];
    this.feedbackImageFiles = [];
    this.feedbackImageFilenames = [];
    this.preloadImage = true;
    this.progressBarValue = 0;
    this.themeStyle = "dark";
    this.steps = 0;
    this.validationsForAssetsSelected = [];
  }

  ngOnInit(): void {
    this.eventInfo = (
      this._authenticationService.currentUserValue as any
    )?.reporter.event;

    if (!this.eventInfo) {
      window.location.href = `/survey/login/OX-1011`;
    } else {
      this.loadSurvey();

      this.formSurvey.valueChanges.subscribe(() => {
        this.validateSteps();
      });
  
      this._coreConfigService.config
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((config) => {
          this.coreConfig = config;
        });
    }
  }

  loadSurvey() {
    this._surveyService
      .getSurveyByReporter(
        this.eventInfo.reportUrl,
        (this._authenticationService.currentUserValue as any)?.reporter.id
      )
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError((error) => {
          return of(error);
        }),
        )
      .subscribe((survey: any) => {
        if (survey.error) {
          if (survey.error.message === 'The survey has already been sent') {
            this._authenticationService.logout('reporter');
            window.location.href = `/survey/login/OX-1011`;
          }
        } else {
          this.steps = survey.categories.length;
          this.isSurveyExtended = survey.surveyExtended;
          this.projectData = {
            image: survey.projectImage,
            name: survey.projectName,
          };
          this.eventData = {
            expectedAttendance: survey.eventExpectedAttendance,
          }
          for (const [index, category] of survey.categories.entries()) {
            this.setCategoryFormControl(category.id, index);

            for (const [qindex, question] of category.questions.entries()) {
              this.setQuestionFormControl(
                question.id,
                question.name,
                index,
                qindex
              );

              if (
                question.questionConfig &&
                question.questionConfig.name === "maximum"
              ) {
                // Feedback always must be at last in array, and contain the comments
                if (index + 1 === survey.categories.length) {
                  this.maxFeedbackImages = [];
                  this.setOptionFormControl(false, index, qindex, question, true);
                } else if (category.behaviour === "LOAD_ASSETS_SELECTED") {
                  this.assetQuestions = category.questions;
                  this.setOptionFormControl(
                    false,
                    index,
                    qindex,
                    question,
                    false
                  );
                } else {
                  // Question with options
                  this.setOptionFormControl(true, index, qindex, question, false);
                }
              } else {
                // Question with options
                this.setOptionFormControl(true, index, qindex, question, false);
              }
            }
          }

          this.survey = survey;
        }
      });
  }

  setCategoryFormControl(categoryId: number, categoryIndex: number) {
    this.formSurvey.setControl(
      `category_${categoryIndex}`,
      this.fb.group({
        id: [categoryId],
        comment: [],
        questions: this.fb.group({}),
      })
    );
  }

  setQuestionFormControl(
    questionId: number,
    questionName: string,
    categoryIndex: number,
    questionIndex: number
  ) {
    (
      this.formSurvey.get(`category_${categoryIndex}.questions`) as FormGroup
    ).setControl(
      `question_${questionIndex}`,
      this.fb.group({
        id: [questionId],
        label: [questionName],
        value: [],
        comment: [],
        options: this.fb.group({}),
      })
    );
  }

  setOptionFormControl(
    withLabel: boolean,
    categoryIndex: number,
    questionIndex: number,
    question: any,
    setMaxFeedbackImages: boolean
  ) {
    const form = this.formSurvey.get(
      `category_${categoryIndex}.questions.question_${questionIndex}.options`
    ) as FormGroup;

    if (
      this.isSurveyExtended &&
      categoryIndex === 2 &&
      question.options.length > 0 &&
      question.options[0].assets
    ) {
      this.assets = question.options[0].assets;
      this.assetQuantity = this.assets.length;
    }

    if (withLabel) {
      for (const [subindex, option] of question.options.entries()) {
        let nameToDisplay = question.name;
        let optionId = question.id;
        if (option.triggerQuestion) {
          nameToDisplay = option.triggerQuestion.name;
          optionId = option.triggerQuestion.id;
        } else if (option.displayType === "DATA") {
          nameToDisplay = option.name;
        }

        form.setControl(
          `option_${subindex}`,
          this.fb.group({
            id: [optionId],
            label: [nameToDisplay],
            value: ["", Validators.required],
            comment: [],
          })
        );
      }
    } else {
      const maxCount =
        this.assetQuantity && categoryIndex === 3
          ? this.assetQuantity
          : question.questionConfig.value;

      for (let i = 0; i < maxCount; i++) {
        let validators = Validators.required;
        if (setMaxFeedbackImages) {
          this.maxFeedbackImages.push(i);
          validators = null;
        }

        if (this.isSurveyExtended && categoryIndex === 3) {
          validators = null;
        }

        form.setControl(
          `option_${i}`,
          this.fb.group({
            id: [question.id],
            label: [],
            value: ["", validators],
            comment: [],
          })
        );
      }
    }
  }

  wizardStepper(action: string) {
    if (action === "back") {
      if (this.isSurveyExtended && this.currentStep === 4) {
        if (this.currentAssetDisplayed > 0) {
          --this.currentAssetDisplayed;
        } else {
          this.currentStep--;
        }
      } else {
        this.currentStep--;
      }
    } else if (action === "forward") {
      if (this.isSurveyExtended && this.currentStep === 4) {
        if (!this.isCurrentStepValid) {
          console.log(
            "No puede continuar hasta completar las preguntas para los assets."
          );
          return;
        }

        if (this.currentAssetDisplayed < this.assetsSelected.length - 1) {
          ++this.currentAssetDisplayed;
        } else {
          this.currentStep++;
        }
      } else {
        if (!this.isCurrentStepValid) {
          console.log("No puede continuar hasta completar las preguntas.");
          return;
        }

        this.currentStep++;
      }
    } else if (action === "finish") {
      if (
        this.feedbackImageFilenames[this.currentFeedbackIndex] &&
        this.feedbackImageFilenames.length < this.maxFeedbackImages.length
      ) {
        const currentElemFeedback = document.getElementById(
          `ox--feedback-section_${this.currentFeedbackIndex}`
        );
        const nextElemFeedback = document.getElementById(
          `ox--feedback-section_${this.currentFeedbackIndex + 1}`
        );

        currentElemFeedback.classList.add("ox--hide-section");
        nextElemFeedback.classList.remove("ox--hide-section");
        this.preloadImage = true;
      }
    }

    this.validateSteps();
  }

  validateSteps() {
    if (this.isSurveyExtended && this.currentStep === 4) {
      this.isCurrentStepValid = this.isAssetQuestionsAnswered();
    } else {
      this.isCurrentStepValid =
        this.formSurvey.controls[`category_${this.currentStep - 1}`].status ===
        "VALID";
    }
  }

  isAssetQuestionsAnswered() {
    const categories = this.formSurvey.controls[
      `category_${this.currentStep - 1}`
    ] as FormGroup;
    const status = this.findStatusForAssetQuestions(categories, 0);
    this.validationsForAssetsSelected = [];

    return !status.includes("INVALID");
  }

  findStatusForAssetQuestions(formGroup: any, level: number): any {
    for (const key in formGroup) {
      if (Object.prototype.hasOwnProperty.call(formGroup, key)) {
        const element = formGroup[key];

        if (key === "controls" || key === "questions" || key === "options") {
          if ("question_0" in element) {
            this.questionsForAssetsSelected = [
              element.question_0,
              element.question_1,
            ];
          }

          const nextLevel = level + 1;
          return this.findStatusForAssetQuestions(element, nextLevel);
        } else if (key === `option_${this.currentAssetDisplayed}`) {
          for (const key2 in element) {
            if (Object.prototype.hasOwnProperty.call(element, key2)) {
              if (key2 === "controls") {
                const nextLevel = level + 1;
                return this.findStatusForAssetQuestions(
                  element[key2],
                  nextLevel
                );
              }
            }
          }
        } else if (key === "value" && !("options" in formGroup)) {
          const lastElement = element as FormGroup;
          if (this.validationsForAssetsSelected.length === 1) {
            this.validationsForAssetsSelected.push(lastElement.status);
            return this.validationsForAssetsSelected;
          }

          const nextLevel = level + 1;
          this.validationsForAssetsSelected.push(lastElement.status);

          return this.findStatusForAssetQuestions(
            this.questionsForAssetsSelected[1],
            nextLevel
          );
        } else if (level === 3) {
          const nextLevel = level + 1;
          return this.findStatusForAssetQuestions(element, nextLevel);
        }
      }
    }
  }

  displayHideQuestion(
    triggerQuestion: any,
    showQuestion: boolean,
    formCategory: string,
    formQuestion: string
  ) {
    if (showQuestion) {
      document.querySelector(".button_no").classList.add("ox--button-active");
      document
        .querySelector(".button_yes")
        .classList.remove("ox--button-active");
      document.getElementById(
        `trigger_question_${triggerQuestion.id}`
      ).style.display = "block";
      this.formSurvey
        .get(`${formCategory}.questions.${formQuestion}.value`)
        .setValue(0);
      this.formSurvey
        .get(`${formCategory}.questions.${formQuestion}.options.option_0.value`)
        .setValue(0);
    } else {
      document
        .querySelector(".button_no")
        .classList.remove("ox--button-active");
      document.querySelector(".button_yes").classList.add("ox--button-active");
      document.getElementById(
        `trigger_question_${triggerQuestion.id}`
      ).style.display = "none";
      this.formSurvey
        .get(`${formCategory}.questions.${formQuestion}.value`)
        .setValue(100);
      this.formSurvey
        .get(`${formCategory}.questions.${formQuestion}.options.option_0.value`)
        .setValue(100);
    }
  }

  selectAssets(asset: any, formCategory: string, formQuestion: string) {
    const assetExists = this.assetsSelected.findIndex((element) => {
      return element.id === asset.id;
    });

    if (assetExists !== -1) {
      if (this.isSurveyExtended) {
        const approveField = this.formSurvey.get(
          `category_3.questions.question_0.options.option_${assetExists}`
        ) as FormGroup;
        const numberField = this.formSurvey.get(
          `category_3.questions.question_1.options.option_${assetExists}`
        ) as FormGroup;

        approveField.get('value').reset('');
        approveField.get('label').reset();
        approveField.clearValidators();
        approveField.updateValueAndValidity();
        
        numberField.get('value').reset('');
        numberField.get('label').reset();
        numberField.clearValidators();
        numberField.updateValueAndValidity();
      }

      this.assetsSelected.splice(assetExists, 1);
      this.assetsIdsSelected.splice(assetExists, 1);      
    } else {
      this.assetsSelected.push({
        id: asset.id,
        assetName: asset.name,
        assetType: asset.assetType ? asset.assetType.name : "sin type",
        description: asset.description,
        assetObjetive: asset.assetObjetive
          ? asset.assetObjetive.name
          : "sin objetive",
        expectedNumber: asset.expectedQuantity,
      });
      this.assetsIdsSelected.push(asset.id);
    }

    if (this.isSurveyExtended) {
      for (let i = 0; i < this.assetsSelected.length; i++) {
        const approveField = this.formSurvey.get(
          `category_3.questions.question_0.options.option_${i}.value`
        ) as FormGroup;
        const numberField = this.formSurvey.get(
          `category_3.questions.question_1.options.option_${i}.value`
        ) as FormGroup;


        approveField.setValidators([Validators.required]);
        approveField.updateValueAndValidity();
        numberField.setValidators([Validators.required]);
        numberField.updateValueAndValidity();
      }
    }

    // Save assets selected for category-question value
    this.formSurvey
      .get(`${formCategory}.questions.${formQuestion}.options.option_0.value`)
      .setValue(this.assetsSelected);
  }

  approveAsset(
    value: any,
    label: string,
    formCategory: string,
    questionNumber: string,
    optionNumber: number
  ) {
    this.formSurvey
      .get(
        `${formCategory}.questions.question_${questionNumber}.options.option_${optionNumber}.label`
      )
      .setValue(label);
    this.formSurvey
      .get(
        `${formCategory}.questions.question_${questionNumber}.options.option_${optionNumber}.value`
      )
      .setValue(value);

    this.formSurvey
      .get(
        `${formCategory}.questions.question_${
          questionNumber + 1
        }.options.option_${optionNumber}.label`
      )
      .setValue(label);
  }

  displayImagePreview(event: any, imageIndex: number) {
    const files = event.target.files;
    const reader = new FileReader();
    const mimeType = files[0].type;
    this.currentFeedbackIndex = imageIndex;

    if (files.length === 0) {
      return;
    }

    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.feedbackImages[imageIndex] = reader.result;
      this.feedbackImageFiles[imageIndex] = files[0];
    };
  }

  uploadImage(formPath: string, imageIndex: number) {
    this.progressBarValue = 0;
    this.preloadImage = false;
    this.isUploadingImage = true;
    const setTimerBar = setInterval(() => {
      this.progressBarValue++;
    }, 900);

    const form = this.formSurvey.get(formPath);
    const fileData = new FormData();
    const fileLoaded: any = this.feedbackImageFiles[imageIndex];

    fileData.append("image", fileLoaded, fileLoaded.name);

    if (fileLoaded.size > 10000000) {
      this._toastService.show("The file uploaded exceed weight parameters", {
        autohide: true,
        headerTitle: "Error",
        icon: "alert-circle",
        iconColorClass: "text-danger",
      });

      form.get(`value`).reset();
      return;
    }

    if (fileLoaded) {
      this._surveyService
        .upload(fileData)
        .pipe(
          catchError((err) => {
            return of(err);
          })
        )
        .subscribe(async (f: File | any) => {
          if (f.error) {
            this._toastService.show(
              "There was an error, please try again later",
              {
                autohide: true,
                headerTitle: "Error",
                icon: "alert-circle",
                iconColorClass: "text-danger",
              }
            );
            this.isUploadingImage = false;
            const progressBar = document.getElementById('ox--progress-bar');
            progressBar.classList.add('error-loading')
          } else {
            this.progressBarValue = 100;
            await this.timeout(500);
            form.get("value").setValue(f.filename);
            this.feedbackImageFilenames[imageIndex] = f.filename;
            this.isUploadingImage = false;
          }
          clearInterval(setTimerBar);
        });
    }
  }

  submit() {
    if (this.isSavingSurvey) {
      return;
    }

    let survey = null;
    const categories = [];

    for (const key in this.formSurvey.value) {
      if (Object.prototype.hasOwnProperty.call(this.formSurvey.value, key)) {
        const category = this.formSurvey.value[key];
        const questions = [];

        for (const subkey in category.questions) {
          if (
            Object.prototype.hasOwnProperty.call(category.questions, subkey)
          ) {
            this.setAnswers(category, subkey, questions);
          }
        }

        categories.push({
          id: category.id,
          comment: category.comment,
          questions,
        });
      }
    }

    survey = {
      event: this.eventInfo.reportUrl,
      reporter: (this._authenticationService.currentUserValue as any).reporter
        .id,
      categories,
    };
    this.saveSurvey(survey);
  }

  setAnswers(category: any, subkey: string, questions: any[]) {
    const question = category.questions[subkey];
    for (const subkey2 in question.options) {
      if (Object.prototype.hasOwnProperty.call(question.options, subkey2)) {
        const option = question.options[subkey2];

        questions.push({
          id: option.id,
          label: option.label,
          value: question.value ? question.value : option.value,
          comment: option.comment,
        });
      }
    }

    if (Object.keys(question.options).length === 0) {
      questions.push({
        id: question.id,
        label: question.label,
        value: question.value,
        comment: question.comment,
      });
    }

    return questions;
  }

  saveSurvey(payload: any) {
    this.isSavingSurvey = true;

    this._surveyService.saveSurvey(payload).subscribe(() => {
      this._router.navigate(["/survey/end", this.eventInfo.reportUrl]);
      this.isSavingSurvey = false;
    });
  }

  changeTheme() {
    if (this.themeStyle === "dark") {
      this.themeStyle = "white";
    } else {
      this.themeStyle = "dark";
    }
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
