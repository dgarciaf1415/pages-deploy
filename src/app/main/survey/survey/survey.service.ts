import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { environment } from "environments/environment";
import { ToastService } from "app/layout/components/toast/toast.service";

@Injectable({
  providedIn: "root",
})
export class SurveyService {
  apiUrl: string;
  surveyId: number;

  constructor(private http: HttpClient, private _toastService: ToastService) {
    this.apiUrl = `${environment.apiUrl}/surveys`;
    this.surveyId = 2;
  }

  getSurvey() {
    const url = `${this.apiUrl}/${this.surveyId}/questions`;
    return this.http.get(url);
  }

  getEventInfo(eventUrl: string){
    const url = `${this.apiUrl}/event/${eventUrl}`;
    return this.http.get(url);
  }

  getSurveyByReporter(eventUrl: string, reporterId: number) {
    const url = `${this.apiUrl}/reporter/${reporterId}/${eventUrl}`;
    return this.http.get(url);
  }

  saveSurvey(payload: any) {
    const url = `${this.apiUrl}/answers`;
    return this.http.post(url, payload).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  saveSurveyComments(payload: any) {
    const url = `${this.apiUrl}/comments`;
    return this.http.post(url, payload).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }

  upload(file: any) {
    const url = `${this.apiUrl}/attachments`;
    return this.http.post(url, file).pipe(
      catchError((error: HttpErrorResponse) => {
        this._toastService.show(error.error.message, {
          autohide: false,
          headerTitle: 'Error',
          icon: 'alert-circle',
          iconColorClass: 'text-danger'
        });

        return throwError(error);
      })
    );
  }
}
