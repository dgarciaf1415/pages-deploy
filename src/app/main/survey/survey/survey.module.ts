import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { NouisliderModule } from "ng2-nouislider";

import { UtilsModule } from 'app/utils/utils.module';
import { SurveyComponent } from './survey.component';
import { SurveyService } from './survey.service';

@NgModule({
  declarations: [
    SurveyComponent
  ],
  imports: [
    CommonModule,
    CoreCommonModule,
    NgbModule,
    CoreDirectivesModule,
    NouisliderModule,
    UtilsModule,
  ],
  providers: [SurveyService],
})
export class SurveyModule { }
