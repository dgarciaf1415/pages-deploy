import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from 'app/auth/service';
import { Subject, of } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

import { WelcomeService } from './welcome.service';
import { environment } from 'environments/environment';
import { CoreConfigService } from '@core/services/config.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WelcomeComponent implements OnInit {
  public coreConfig: any;
  public eventType: any;
  public imageContainer: string;
  public reporterId: number;
  public reportUrl: string;
  public supportEmail: string;
  public surveyType: string;
  public welcomeText: string;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private _authenticationService: AuthenticationService, private _welcomeService: WelcomeService, private _coreConfigService: CoreConfigService) {
    this._unsubscribeAll = new Subject();
    this.imageContainer = environment.spaceUrl;
    this.supportEmail = 'support@onxite.com';
    this.reportUrl = (
      this._authenticationService.currentUserValue as any
    ).reporter.event.reportUrl;
    this.reporterId = (
      this._authenticationService.currentUserValue as any
    ).reporter.id;
  }

  ngOnInit(): void {
    this._welcomeService
      .getSurveyType(this.reportUrl, this.reporterId)
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError((error) => {
          return of(error);
        },)
        )
      .subscribe((res: any) => {
        if (res.error) {
          if (res.error.message === 'The survey has already been sent') {
            this._authenticationService.logout('reporter');
          }
        } else {
          this.eventType = res;
          this.surveyType = res.surveyType.toLowerCase();

          if (this.surveyType === 'extended') {
            this.welcomeText = `Your survey report for: [${res.eventName}] is ready.`;
          } else {
            this.welcomeText = `Thank you for being part of [${res.eventName}] reporting & activation crew`;
          }
        }
      });

    this._coreConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.coreConfig = config;
      });
  }

  startSurvey() {
    window.location.href = `/survey/event/${this.reportUrl}`;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
