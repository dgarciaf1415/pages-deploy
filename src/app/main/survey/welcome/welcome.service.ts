import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WelcomeService {
  public apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = `${environment.apiUrl}/surveys`;
  }

  getSurveyType(eventUrl: string, reporterId: number) {
    const url = `${this.apiUrl}/event/${eventUrl}/${reporterId}`;
    return this.http.get(url);
  }
}
