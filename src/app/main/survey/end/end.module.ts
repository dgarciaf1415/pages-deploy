import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreCommonModule } from '@core/common.module';
import { CoreDirectivesModule } from '@core/directives/directives';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { EndComponent } from './end.component';

@NgModule({
  declarations: [
    EndComponent
  ],
  imports: [
    CommonModule,
    CoreCommonModule,
    NgbModule,
    CoreDirectivesModule,
  ]
})
export class EndModule { }
