import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Subject } from "rxjs";

import { AuthenticationService } from "app/auth/service/authentication.service";
import { SurveyService } from "../survey/survey.service";
import { environment } from "environments/environment";
import { CoreConfigService } from "@core/services/config.service";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-end",
  templateUrl: "./end.component.html",
  styleUrls: ["./end.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class EndComponent implements OnInit {
  public coreConfig: any;
  public emailContact: string;
  public eventInfo: any;
  public imageContainer: string;
  public reportUrl: string;

  //private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _authenticationService: AuthenticationService,
    private _surveyService: SurveyService,
    private _coreConfigService: CoreConfigService
  ) {
    this._unsubscribeAll = new Subject();
    this.imageContainer = environment.spaceUrl;
    this.emailContact = 'word@onxite.com';

    this.reportUrl = (
      this._authenticationService.currentUserValue as any
    ).reporter.event.reportUrl;
  }

  ngOnInit(): void {
    this._surveyService
      .getEventInfo(this.reportUrl)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res: any) => {
        this.eventInfo = res;
      });

    this._coreConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.coreConfig = config;
      });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
