import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { SurveyComponent } from "./survey/survey.component";
import { EndComponent } from "./end/end.component";
import { LoginModule } from "./login/login.module";
import { AuthReporterGuard } from "app/auth/helpers/auth-reporter.guards";
import { NoAuthGuard } from "app/auth/helpers/no-auth.guards";
import { SurveyModule as SurveysModule } from "./survey/survey.module";
import { EndModule } from "./end/end.module";
import { WelcomeComponent } from "./welcome/welcome.component";
import { WelcomeModule } from "./welcome/welcome.module";

const routes: Routes = [
  {
    path: "login/:id",
    component: LoginComponent,
    data: { animation: "misc" },
    canActivate: [NoAuthGuard],
  },
  {
    path: "welcome/:id",
    component: WelcomeComponent,
    data: { animation: "misc" },
    canActivate: [AuthReporterGuard],
  },
  {
    path: "event/:id",
    component: SurveyComponent,
    data: { animation: "misc" },
    canActivate: [AuthReporterGuard],
  },
  {
    path: "end/:id",
    component: EndComponent,
    data: { animation: "misc" },
    canActivate: [AuthReporterGuard],
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WelcomeModule,
    LoginModule,
    SurveysModule,
    EndModule,
  ],
  providers: [],
})
export class SurveyModule {}
