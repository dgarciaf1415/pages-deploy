export const environment = {
  production: true,
  hmr: false,
  apiUrl: 'https://api.onxite.com',
  spaceUrl: 'https://space-onxite.nyc3.digitaloceanspaces.com/production',
  adminUrl: 'https://app.onxite.com',
  maxDaysSinceEnd: 2190,
  enableLocalStorage: false,
};